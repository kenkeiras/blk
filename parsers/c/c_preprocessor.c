#ifndef C_PREPROCESSOR_C
#define C_PREPROCESSOR_C

// Align correctly the produced text
#define CARE_OF_STYLE

// Top conditional depth
#define MAX_IF_DEPTH 256
    
#define DISCARD_LINE(f, c) \
    while((c = fgetc(f)) != '\n'){ \
        if (c == EOF){ \
            break; \
        } \
    }

#include "c_preprocessor.h"
#include "hash_table.h"
#include "linked_list.h"
#include "c_tokens.h"
#include <assert.h>
/**
 * @file c_preprocessor.c
 *
 * @brief C preprocessing engine.
 * 
 */
 
/**
 * C preprocessing manager data type.
 * 
 */
struct c_preprocessor{
    hash_table_t object_definitions;
    hash_table_t function_definitions;
    char reading[MAX_IF_DEPTH]; // Last If was True
    char read[MAX_IF_DEPTH]; // Some If was True
    int depth; // #if/#else depth
    int unread_depth; // depth inside a unread #if
};


struct object_definition{
    llist_t *list;
};

struct function_definition{
    int param_num;
    llist_t *list;
};


/**
 * Description: Free a token list.
 * 
 * @param tl The token list to free.
 * 
 */
void free_object_definition(struct object_definition* obj){
    free_llist(obj->list, (void (*) (void*)) free_token_ptr);
    free(obj);
}


/**
 * Description: Free a macro definition.
 * 
 * @param f The function macro to free.
 * 
 */
void free_function_definition(struct function_definition* f){
    free_llist(f->list, (void (*) (void*)) free_token_ptr);
    free(f);
}


/**
 * Description: Converts a string to lower case.
 * 
 * @param s The string to be converted.
 * 
 */
void to_lower_case(char *s){
    if (s != NULL){
        int i;
        for (i = 0; s[i] != '\0'; i++){
            if ((s[i] >= 'A') && (s[i] <= 'Z')){
                s[i] += 0x20; // Gap between upper and lowercase.
            }
        }
    }
}


/**
 * Description: Returns a linked list of paths which hold standard library
 *  headers. Delete with `free_llist(l, (void (*) (void *)) free);`.

 * 
 * @return A linked list of header directories.
 * 
 */
llist_t *get_include_paths(){
    llist_t *l = create_llist(); 

    add_to_llist(l, (void *) strdup(DEFAULT_INCLUDE_PATH));
    /* //Save current directory
    char *cwd = getcwd(NULL, 0);

    chdir(BASE_INCLUDE_PATH);

    // Restore original directory and return the list
    assert(chdir(cwd) != -1);
    free(cwd);    
    */
    return l;
}    
    

/**
 * Description: Creates a new c preprocessor.
 * 
 * @return A c preprocessor structure.
 * 
 */
c_preprocessor_t* create_c_preprocessor(){
    c_preprocessor_t *cpp = (c_preprocessor_t *) 
                                        malloc(sizeof(c_preprocessor_t));

    if (cpp == NULL){
        return NULL;
    }
    
    cpp->object_definitions = create_hash_table();
    cpp->function_definitions = create_hash_table();
    cpp->depth = -1;
    cpp->unread_depth = 0;
    
    return cpp;
}


/**
 * Description: Frees the memory allocated for a c preprocessor.
 *
 * @param cpp The C preprocessor to be freed. 
 *
 */
void free_c_preprocessor(c_preprocessor_t* cpp){
    assert(cpp != NULL);
    assert(cpp->object_definitions != NULL);
    assert(cpp->function_definitions != NULL);
    
    free_hash_table(cpp->object_definitions, 
                                        (void (*) (void *)) free_object_definition, 
                                        (void (*) (char *)) free);

    free_hash_table(cpp->function_definitions,
                                   (void (*) (void *)) free_function_definition, 
                                   (void (*) (char *)) free);
    free(cpp);
}


/**
 * Description: reads all the requested data from a file (unless it ends).
 * 
 * @param buff Buffer to read to.
 * @param size The size of each element to be read.
 * @param n    The number of elements to read.
 * @param f    The file to read from.
 * 
 * @return The number of elements read.
 * 
 */
int freadall(void *buff, int size, int n, FILE *f){
    int i, rem = n;
    while ((rem > 0) && (!feof(f))){
        i = fread(buff, sizeof(char), rem, f);
        rem -= i;
    }
    
    return n - rem;
}


/**
 * Description: checks whether or not the last preprocessor line has ended.
 * 
 * @param cpp The C preprocessor structure.
 * 
 * @return 0 if it has ended, 1 otherwise.
 * 
 */
int is_buffering(c_preprocessor_t *cpp){
    return 0;
} 


/**
 * Description: Execute a #include preprocessor sentence.
 * 
 * @param cpp       The C preprocessor structure.
 * @param f         File description containing the <file> | "file" line to include.
 * @param pd        A preprocessed data structure to insert the data.
 * @param inc_paths Include file paths.
 * 
 */
void preprocess_include(c_preprocessor_t *cpp, 
                        FILE *f,
                        int *line,
                        preprocessed_data_t *pd,
                        llist_t* inc_paths){

    llist_t* tl = read_token_line(f, line, 0);
    char *fname = NULL;
    FILE *inc_file = NULL;

    assert(tl->first != NULL);

    // #include <file>
    if (((c_token*)tl->first->element)->type == LT){
        assert(((c_token*)tl->last->element)->type == GT);

        int size = 0;
        char *tmp, *tmp2;

        llist_node_t *node = tl->first->next;

        // Count characters
        while((node != NULL) && (node != tl->last)){
            c_token t = *((c_token*) node->element);
            switch(t.representation){
                case CHARACTER_R:
                    size++;
                    break;

                case STRING_R:
                    if (t.content.s != NULL){
                        size += strlen(t.content.s);
                    }
                    break;

                case INTEGER_R:
                    fprintf(stderr, "Unexpected token %i\n", t.content.i);
                    abort();
                    break;

                case FLOAT_R:
                    fprintf(stderr, "Unexpected token %f\n", t.content.f);
                    abort();
                    break;
            }
            
            node = node->next;
        }

        // Build string
        tmp = tmp2 = (char *)malloc(sizeof(char) * (size + 1));
        assert(tmp != NULL);

        node = tl->first->next;

        // Count characters
        while((node != NULL) && (node != tl->last)){
            c_token t = *((c_token*) node->element);
            switch(t.representation){
                case CHARACTER_R:
                    tmp2[0] = t.content.c;
                    tmp2 = &tmp2[1];
                    break;

                case STRING_R:
                    if (t.content.s != NULL){
                        strcpy(tmp2, t.content.s);
                        tmp2 = &tmp2[strlen(t.content.s)];
                    }
                    break;
                
                default:
                    break;
            }
            node = node->next;
        }
        fname = tmp;

        // Save current path
        char *cwd = getcwd(NULL, 0);

        // Check all paths
        node = inc_paths->first;
        while ((inc_file == NULL) && (node != NULL)){
            if (chdir((char*)node->element) == 0){

                // Try to open file
                inc_file = fopen(fname, "rt");
                if (inc_file != NULL){

                    // Build string
                    tmp = (char*) malloc(sizeof(char) *
                                            (strlen((char*)node->element) + 
                                             size + 1));

                    assert(tmp != NULL);
                    sprintf(tmp, "%s%s", (char*)node->element, fname);

                    free(fname);
                    fname = tmp;
                }
            }
            else{
                perror((char*)node->element);
            }

            node = node->next;
        }
        assert(chdir(cwd) == 0);
        free(cwd);
    }
    // #include "file"
    else if (((c_token*)tl->first->element)->type == STRING){
        assert(tl->first->next == NULL);
        fname = strdup(((c_token*)tl->first->element)->content.s);
        inc_file = fopen(fname, "rt");
    }
    // #include algo extraño :P 
    else{
        fprintf(stderr, "Error, token '");
        print_token(stderr, *((c_token*)tl->first->element));
        fprintf(stderr, "' unknown\n");
    }

    // Free tokens
    free_llist(tl, (void(*)(void*))free_token_ptr);

    // Include file
    if (inc_file != NULL){
        fclose(inc_file);

        preprocessed_data_t* inc_pd = preprocess_file(cpp, fname, inc_paths);
        merge_llists(pd, inc_pd);
        //printf("%s\n", fname);
    }
    else{
        fprintf(stderr, "File `%s' not found\n", fname != NULL? fname: "");
    }
    
    // Free file
    if (fname != NULL){
        free(fname);
    }
}


/**
 * Description: preprocesses a #define line.
 * 
 * @param cpp The C preprocessor structure.
 * @param f   File to preprocess.
 * @param line Current line number.
 * 
 */
void add_definition(c_preprocessor_t* cpp, FILE* f, int *line){
    int c;
    
    while (is_whitespace(c = fgetc(f))){
        if ((c == '\n') || (c == EOF)){
            fprintf(stderr, "Premature end of definition?\n");
        }
    }
    ungetc(c, f);

    c_token tok = read_token(f, *line, 0);
    assert(tok.representation == STRING_R);

    llist_t* tl = read_token_line(f, line, 0);
 
    if (c == '('){
        int i = 0,
            param_num = 0;

        llist_node_t *node = tl->first;

        assert(node != NULL);
        c_token* curr = (c_token*) node->element;

        llist_t* list = create_llist();
        hash_table_t ht = create_hash_table(10);
        while ((node != NULL) && (curr->type != CLOSE_PARENS)){
            if (curr->type == COMMA){
                i++;
            }
            else{
                assert(curr->representation == STRING_R);
                insert_hash_table(ht, curr->content.s, (void *) i);
                curr->content.s = NULL;
            }
            free_token_ptr(curr);

            node = node->next;
            if (node != NULL){
                curr = (c_token*) node->element;
            }
        }        
        
        free_token_ptr(curr);
        node = node->next;
        while (node != NULL){
            curr = (c_token*) node->element;
            
            if (curr->type == VARIABLE){
                assert(curr->representation == STRING_R);
                long index = (long) get_hash_table(ht, curr->content.s);
                //printf("%s - %i\n", curr->content.s, index);
                free(curr->content.s);
                curr->content.i = index;
                curr->representation = INTEGER_R;
                
                param_num++;
            }
            /// @note move line/col correction here?
            add_to_llist(list, curr);
            node = node->next;
        }

        struct function_definition *f = (struct function_definition*) 
                                 malloc(sizeof(struct function_definition));

        assert(f != NULL);
        f->param_num = param_num;
        f->list = list;

        insert_hash_table(cpp->function_definitions, strdup(tok.content.s), f);

        free_hash_table(ht, NULL, (void (*)(char*)) free);
        free_llist(tl, NULL);

    }
    else{
        struct object_definition *obj = (struct object_definition*) 
                                            malloc(sizeof(struct object_definition));

        assert(obj != NULL);
        obj->list = tl;

        insert_hash_table(cpp->object_definitions, strdup(tok.content.s), obj);
    }
    free_token(tok);

}


/**
 * Description: preprocesses a #undef line.
 * 
 * @param cpp The C preprocessor structure.
 * @param f   File to preprocess.
 * @param line Current line number.
 * 
 */
void rm_definition(c_preprocessor_t* cpp, FILE* f, int *line){
    char c;
    while (is_whitespace(c = fgetc(f))){
        if ((c == '\n') || (c == EOF)){
            fprintf(stderr, "Premature end of definition?\n");
        }
    }
    ungetc(c, f);

    c_token tok = read_token(f, *line, 0);
    assert(tok.representation == STRING_R);

    rm_hash_table(cpp->object_definitions, tok.content.s,
		  (void (*) (void *)) free_object_definition,
		  (void (*) (char *)) free);

    rm_hash_table(cpp->function_definitions, tok.content.s,
		  (void (*) (void *)) free_function_definition,
		  (void (*) (char *)) free);

    free_token(tok);
}


/**
 * Description: Checks if a token is defined in the C preprocessor structure.
 * 
 * @param cpp The C preprocessor structure.
 * @param tok The token to be evaluated.
 * 
 * @return true if it is a macro or object.
 * 
 */
int is_defined(c_preprocessor_t *cpp, c_token tok){
    if (tok.representation == STRING_R){
        struct object_definition* ol = (struct object_definition *)
                                        get_hash_table(
                                                cpp->object_definitions,
                                                tok.content.s);
        if (ol != NULL){
            return 1;
        }
        

        struct function_definition* fd = (struct function_definition *)
                                        get_hash_table(
                                                cpp->function_definitions,
                                                tok.content.s);
        return fd != NULL;
    }
    return 0;
}


/**
 * Description: Substitutes a line tokens according to object definitions.
 * 
 * @param cpp The C preprocessing structure.
 * @param list  The list of tokens to read.
 * 
 * @return A linked list of c_tokens.
 * 
 */
llist_t* substitute_line_objects(c_preprocessor_t *cpp, llist_t* in_list){

    llist_t* out_list = create_llist();
    llist_node_t *node = in_list->first;
    int defined;
    c_token* tok;

    while (node != NULL){
        tok = (c_token*) node->element;

        defined = 0;
        if ((tok->type == VARIABLE) && (tok->representation == STRING_R)){
            struct object_definition* ol = (struct object_definition *) get_hash_table(
                                                    cpp->object_definitions,
                                                    tok->content.s);

            if (ol != NULL){
                defined = 1;
                
                int first = 1,
                    start_column = tok->column,
                    start_line   = tok->line,
                    base_column, base_line;

                llist_node_t *onode = ol->list->first;
                while (onode != NULL){ /// @todo Add __LINE__, __COLUMN__, __FILE__, ...
                    c_token *tmp = copy_token(*((c_token*)onode->element));
                
                    if (first){
                        base_column = tmp->column;
                        base_line   = tmp->line;

                        tmp->column = start_column;
                        tmp->line   = start_line;
                        first = 0;
                    }
                    else{
                        if (tmp->line == base_line){
                            tmp->column = start_column + 
                                                   (tmp->column - base_column);
                        }
                        tmp->line = start_line + (tmp->line - base_line);
                    }
                
                    add_to_llist(out_list, tmp);                    
                    
                    onode = onode->next;
                }
            }
        }
        if (!defined){
            add_to_llist(out_list, copy_token(*tok));
        }

        node = node->next;

    }
    
    return out_list;
}


/**
 * Description: Substitutes a line tokens according to macro definitions.
 * 
 * @param cpp The C preprocessing structure.
 * @param list  The list of tokens to read.
 * 
 * @return A linked list of c_tokens.
 * 
 */
llist_t* substitute_line_macros(c_preprocessor_t *cpp, llist_t* in_list){
    llist_t* out_list = create_llist();
    llist_node_t *node = in_list->first, *next;
    int defined;
    c_token *tok, *curr;

    while (node != NULL){
        tok = (c_token*) node->element;
        defined = 0;

        if (tok->representation == STRING_R){
            next = node->next;
            if ((next != NULL) && (((c_token*)next->element)->type == OPEN_PARENS)){
                struct function_definition* fd = (struct function_definition *)
                                                    get_hash_table(
                                                        cpp->function_definitions,
                                                        tok->content.s);

                if (fd != NULL){
                    defined = 1;

                    llist_node_t *ptr = next->next;

                    assert(ptr != NULL);

                    int depth = 0, param = 0;
                    llist_t** params = NULL;
                    if (fd->param_num > 0){
                        params = (llist_t **) malloc(sizeof(llist_t*) * 
                                                                fd->param_num);
                        bzero(params, sizeof(llist_t*) * fd->param_num);
                        
                        assert(params != NULL);
                        params[0] = create_llist();
                    }
                    curr = (c_token*)ptr->element;
                    
                    // Read the params
                    while ((ptr != NULL)
                       && ((depth > 0) || (curr->type != CLOSE_PARENS))){

                        if(param >= fd->param_num){
                            assert(param < fd->param_num);
                        }

                        switch(curr->type){
                            case OPEN_PARENS:
                                depth++;
                                break;
                            
                            case CLOSE_PARENS:
                                depth--;
                                break;
                            
                            case COMMA:
                                param++;
                                params[param] = create_llist();
                                break;

                            default:
                                add_to_llist(params[param], copy_token(*curr));
                        }

                        ptr = ptr->next;
                        if (ptr != NULL){
                            curr = (c_token*)ptr->element;
                        }
                        //free(tmp);
                    }

                    node = ptr;

                    ptr = fd->list->first;
                    int first = 1,
                        start_column = tok->column,
                        start_line   = tok->line,
                        base_column, base_line;

                    while(ptr != NULL){ /// @todo Fix line/column
                        curr = (c_token*) ptr->element;
                        
                        if ((curr->type == VARIABLE) &&
                            (curr->representation == INTEGER_R)){

                            llist_t *repl = substitute_line_macros(cpp,
                                                       params[curr->content.i]);

                                int sub_first = 1,
                                    sub_start_column = start_column,
                                    sub_start_line   = start_line,
                                    sub_base_column, sub_base_line;
                            
                            #ifdef CARE_OF_STYLE
                                llist_node_t *tmp = repl->first;
                                while(tmp != NULL){
                                    curr = (c_token*) tmp->element;

                                    if (sub_first){
                                        sub_base_column = curr->column;
                                        sub_base_line   = curr->line;

                                        curr->column = sub_start_column;
                                        curr->line   = sub_start_line;
                                        sub_first = 0;
                                        if (first){
                                            base_column = sub_base_column;
                                            base_line = sub_base_line;
                                            first = 0;
                                        }
                                    }                                    
                                    else{
                                        if (curr->line == base_line){
                                            curr->column = start_column + 
                                                (curr->column - base_column);
                                        }
                                        curr->line = start_line +  
                                                       (curr->line - base_line);
                                    }
                                    add_to_llist(out_list, curr);
                                    
                                    tmp = tmp->next;
                                }
                                
                                free_llist(repl, NULL);
                            #else
                                merge_llists(out_list, repl);
                            #endif
                        }
                        else{
                            if (first){
                                base_column = curr->column;
                                base_line   = curr->line;

                                curr->column = start_column;
                                curr->line   = start_line;
                                first = 0;
                            }
                            else{
                                if (curr->line == base_line){
                                    curr->column = start_column + 
                                                   (curr->column - base_column);
                                }
                                curr->line = start_line +
                                                   (curr->line - base_line);
                            }

                            add_to_llist(out_list, copy_token(*curr));
                        }

                        ptr = ptr->next;
                    }

                    // Free the data structures
                    int i;
                    for (i = 0; i < fd->param_num;i++){
                        free_llist(params[i], (void(*)(void*)) free_token_ptr);
                    }
                    free(params);

                }

            }
        }

        if (!defined){
            add_to_llist(out_list, copy_token(*tok));
        }
        
        node = node->next;
    }

    return out_list;
}


/**
 * Description: Substitutes a line tokens according to definitions.
 * 
 * @param cpp The C preprocessing structure.
 * @param pd  The preprocessed data list.
 * @param line_num The line where the tokens stay.
 * @param list  The list of tokens to read.
 * 
 */
void substitute_line(c_preprocessor_t *cpp,
                     preprocessed_data_t *pd,
                     int line_num,
                     llist_t* list){

    llist_t *obj_list = substitute_line_objects(cpp, list);
    llist_t *final_list = substitute_line_macros(cpp, obj_list);

    free_llist(obj_list, (void (*)(void*)) free_token_ptr);
    merge_llists(pd, final_list);

}


/**
 * Description: passes a single line through a C preprocessor.
 * 
 * @param cpp       The C preprocessor structure.
 * @param fname     The name of the file where the line belongs
 * @param f         File to preprocess.
 * @param line_num  The number of the line.
 * @param pd        A preprocessed data structure to insert the data.
 * @param inc_paths #Include file paths.
 *
 * @return Line where it stopped.
 */
int preprocess_line(c_preprocessor_t *cpp,
                     char *fname,
                     FILE *f,
                     int   line_num,
                     preprocessed_data_t *pd,
                     llist_t* inc_paths){

    char c = fgetc(f);
    int col = 0;
    llist_t* l = NULL;

    while ((is_whitespace(c)) && (c != '\n')){
        col++;
        c = fgetc(f);
    }

    // Not a preprocessor line
    if (c != '#'){
        if((cpp->depth == -1) || (cpp->reading[cpp->depth])){
            ungetc(c, f);
            l = read_token_line(f, &line_num, col);

            substitute_line(cpp, pd, line_num, l);
            
            free_llist(l, (void(*)(void*)) free_token_ptr);
        }
        else{
            DISCARD_LINE(f, c);
            line_num++;
        }
    }
    // Preprocessor line
    else{
        int col = 1;
        c = fgetc(f);
        while((is_whitespace(c)) && (c != '\n')){
            col++;

            c = fgetc(f);            
        }
        if (c == '\n'){
            printf("\n");
            return line_num + 1;
        }
        
        ungetc(c, f);
        c_token tok = read_token(f, line_num, col);
        line_num = tok.line_end;
        
        
        // Preprocessor operations
        switch(tok.type){
            case INCLUDE:
                if ((cpp->depth == -1) || (cpp->reading[cpp->depth])){
                    preprocess_include(cpp, f, &line_num, pd, inc_paths);
                }
                else{
                    DISCARD_LINE(f, c);
                }
                break;

            case DEFINE:
                if ((cpp->depth == -1) || (cpp->reading[cpp->depth])){
                    add_definition(cpp, f, &line_num);
                }
                else{
                    DISCARD_LINE(f, c);
                }
                break;

            case UNDEFINE:
                if ((cpp->depth == -1) || (cpp->reading[cpp->depth])){
                    rm_definition(cpp, f, &line_num);
                }
                else{
                    DISCARD_LINE(f, c);
                }
                break;
            
            case IFDEF:
            case IFNDEF:

                if ((cpp->depth == -1) || (cpp->reading[cpp->depth])){
                    cpp->depth++;
                    assert(cpp->depth < MAX_IF_DEPTH);
                    
                    l = read_token_line(f, &line_num, col);
                    assert(l->first != NULL);
                    int is_true = is_defined(cpp, *((c_token*)l->first->element));
                    if (tok.type == IFNDEF){
                        is_true = !is_true;
                    }
                    
                    cpp->reading[cpp->depth] = cpp->read[cpp->depth] = is_true;
                    
                    free_llist(l, (void(*)(void*)) free_token_ptr);
                }
                else{
                    DISCARD_LINE(f, c);
                    cpp->unread_depth++;
                }
                break;
            
            case ELSE:
                if (cpp->depth == -1){
                    fprintf(stderr, "%s:%i:Found #else before #if\n", fname,
                                                                  line_num + 1);
                }
                else if (cpp->unread_depth == 0){
                    cpp->reading[cpp->depth] = !cpp->read[cpp->depth];
                    if (cpp->reading[cpp->depth]){
                        cpp->read[cpp->depth] = 1;
                    }
                }
                DISCARD_LINE(f, c);
                break;
                
            case ENDIF:
                if (cpp->depth == -1){
                    fprintf(stderr, "%s:%i:Found #endif before #if\n", fname,
                                                                  line_num + 1);
                    //abort();
                }
                else{
                    if (cpp->unread_depth == 0){
                        cpp->depth--;
                    }
                    else{
                        cpp->unread_depth--;
                    }
                }
                DISCARD_LINE(f, c);
                break;

            case WARNING:
                if ((cpp->depth == -1) || (cpp->reading[cpp->depth])){
                    fprintf(stderr, "%s:%i:Warning: ", fname, line_num + 1);
                    while((c = fgetc(f)) != '\n'){
                        fputc(c, stderr);
                    }
                    fputc('\n', stderr);
                }
                else{
                    DISCARD_LINE(f, c);
                }
                break;
                
            case ERROR:
                if ((cpp->depth == -1) || (cpp->reading[cpp->depth])){
                    fprintf(stderr, "%s:%i:Error: ", fname, line_num + 1);
                    while((c = fgetc(f)) != '\n'){
                        fputc(c, stderr);
                    }
                    fputc('\n', stderr);
                    abort();
                }
                else{
                    DISCARD_LINE(f, c);
                }
                break;

            default:
                fprintf(stderr, "%s:%i:Unexpected token ", fname, line_num + 1);
                print_token(stderr, tok);
                fprintf(stderr, "\n");
                DISCARD_LINE(f, c);
        }
        free_token(tok);
    }

    if (feof(f)){
        return -1;
    }
    else{
        return line_num + 1; // Sic, increment line
    }
}


/**
 * Description: passes a file through a C preprocessor.
 * 
 * @param cpp   The C preprocessor structure.
 * @param fname The name of the file to be preprocessed.
 * @param inc_paths #Include file paths.
 * 
 * @return The preprocessed data of the file.
 * 
 */
preprocessed_data_t* preprocess_file(c_preprocessor_t *cpp,
                                     char *fname,
                                     llist_t *inc_paths){
    preprocessed_data_t *pd;
    FILE *f = fopen(fname, "rt");
    if (f == NULL){
        fprintf(stderr, "Error reading %s\n", fname);
        return NULL;
    }

    pd = create_llist();
    assert(pd != NULL);
    
    /* ---------------------------------------- */

    int line_num = 0;

    do{
        line_num = preprocess_line(cpp, fname, f, line_num, pd, inc_paths);

    } while(line_num != -1);
    /* ---------------------------------------- */

    fclose(f);
    return pd;
}


/**
 * Description: frees the memory allocated for a preprocessed data structure.
 * 
 * @param pd The preprocessed data to be freed.
 * 
 */
void free_preprocessed_data(preprocessed_data_t* pd){
    if (pd == NULL){
        return;
    }

    free_llist(pd, (void (*) (void *)) free_token_ptr);
} 


/**
 * Description: stores preprocessed data in a file.
 * 
 * @param f The file where the info is going to be stored.
 * @param pd The preprocessed data to be stored.
 * 
 */
void output_preprocessed(FILE *f, preprocessed_data_t* pd){
    if (pd == NULL){
        return;
    }

    int i, last_line = 1, last_col = 0;
    llist_node_t *node = pd->first;
    c_token* data;

    while (node != NULL){
        
        data = (c_token*) node->element;

        for (i = last_line; i < (data->line); i++){
            fprintf(f, "\n");
            last_col = 0;
        }
        last_line = i;
        for (i = last_col; i < (data->column); i++){
            fprintf(f, " ");
        }
        last_col = data->column_end + 1;

        print_token(f, *data);

        node = node->next;
    }
    puts("");
}


#ifdef STANDALONE
int main(int argc, char **argv){
    if (argc < 2){
        printf("%s <file.c> [<file.c> [...]]\n", argc == 1?
                                    argv[0] : "c_preprocessor");
        exit(0);
    }
    int i;
    
    c_preprocessor_t *cpp = create_c_preprocessor();
    llist_t *inc_paths = get_include_paths();
    assert(cpp != NULL);

    for (i = 1; i < argc;i++){
        preprocessed_data_t* pd;
        pd = preprocess_file(cpp, argv[i], inc_paths);

        output_preprocessed(stdout, pd);

        free_preprocessed_data(pd);
    }
    free_llist(inc_paths, (void (*) (void *)) free);
    free_c_preprocessor(cpp);

    return 0;
}
    
#endif
#endif
