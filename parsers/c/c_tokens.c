#ifndef C_TOKENS_C
#define C_TOKENS_C

#include "c_tokens.h"
#include <assert.h>
#include <string.h>
#include <stdlib.h>

/**
 * Description: Frees a allocated token.
 * 
 * @param token The allocated token to free the content from.
 * 
 */
void free_token_ptr(c_token* token){
    if (token == NULL){
        return;
    }
    
    free_token(*token);
    free(token);
}


/**
 * Description: Duplicates the data inside a token pointer.
 * 
 * @param token The data to duplicate
 * 
 * @return A new token pointer with the duplicate data.
 * 
 */
c_token* copy_token(c_token token){
    c_token* ctoken = (c_token*) malloc(sizeof(c_token));
    if (ctoken == NULL){
        return NULL;
    }
    
    *ctoken = token;
    if ((token.representation == STRING_R) && (token.content.s != NULL)){
        ctoken->content.s = strdup(token.content.s);
    }
    
    return ctoken;
}


/**
 * Description: Returns a escaped version of the string.
 * 
 * @param s The string to escape.
 * 
 * @return The escaped version of the string (must be manually freed).
 * 
 */
char *escape_string(char *s){
    int size = strlen(s);
    int increments = 32;  // Steps for realloc'ing the string
    
    char *e = (char*) malloc(sizeof(char) * (size + 1));
    assert(e != NULL);
    
    int i;
    for (i = 0; s[i] != '\0'; i++){
        if (i + 3 >= size){ // s[i] = '\\'; s[i] = c; s[i + 1] = '\0';
            size += increments;
            e = (char *) realloc(e, size);
            assert(e != NULL);
        }

        switch(s[i]){
            case '"':
                e[i] = '\\';
                e[i + 1] = '"';
                i++;
                break;
            
            case '\b':
                e[i] = '\\';
                e[i] = 'b';
                i++;
                break;
                    
            case '\n':
                e[i] = '\\';
                e[i] = 'n';
                i++;
                break;
                
            case '\r':
                e[i] = '\\';
                e[i] = 'r';
                i++;
                break;
                
            case '\t':
                e[i] = '\\';
                e[i] = 't';
                i++;
                break;

            default:
                e[i] = s[i];
                
        }
    }
    e[i] = '\0';
    
    return e;
}


/**
 * Description: Writes the textual representation of token on a file.
 * 
 * @param f     File to read to.
 * @param token The token to represent.
 * 
 */
void print_token(FILE* f, c_token token){
    if (token.type == STRING){
        assert(token.representation == STRING_R);
        char *s = escape_string(token.content.s);
        fprintf(f, "\"%s\"", s);
        free(s);
    }
    else{
        switch(token.representation){
            case STRING_R:
                if (token.content.s != NULL){
                    fprintf(f, "%s", token.content.s);
                }
                break;
            
            case CHARACTER_R:
                fprintf(f, "%c", token.content.c);
                break;
                
            case INTEGER_R:
                fprintf(f, "%i", token.content.i);
                break;

            case FLOAT_R:
                fprintf(f, "%f", token.content.f);
                break;
        }
    }
}


/**
 * Description: Frees a token content.
 * 
 * @param token The token to free the content from.
 * 
 */
void free_token(c_token token){
    switch(token.representation){
        case STRING_R:
            if (token.content.s != NULL){
                free(token.content.s);
            }
            break;
        
        default:
            break;
    }
}


/**
 * Description: Checks if a char is a whitespace.
 * 
 * @param c The char to evaluate.
 * 
 * @return True if c is whitespace, else False.
 * 
 */
int is_whitespace(char c){
    switch(c){
        case ' ': case '\n': case '\r': case '\t':
            return 1;

        default:
            return 0;
    }
}


/**
 * Description: Checks if a character is alphanumeric or a underscore.
 * 
 * @param c The character to check.
 * 
 * @return True if is alphanumeric or underscore.
 * 
 */
int is_alphanumeric_under(char c){
    switch(c){
        case '_':
        case '0': case '1': case '2': case '3': case '4': case '5': case '6':
        case '7': case '8': case '9':

        case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g':
        case 'h': case 'i': case 'j': case 'k': case 'l': case 'm': case 'n':
        case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u':
        case 'v': case 'w': case 'x': case 'y': case 'z':
        
        case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G':
        case 'H': case 'I': case 'J': case 'K': case 'L': case 'M': case 'N':
        case 'O': case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U':
        case 'V': case 'W': case 'X': case 'Y': case 'Z':
        
            return 1;
            
        default:
            return 0;
    }
}


/**
 * Description: Checks if `read` concatenated to the data to be read from `f`
 *  matches the `expected` one.
 * 
 * @param read Already read data.
 * @param count Number of bytes already counted.
 * @param expected Expected string.
 * @param tok Source token.
 * @param f File to read data from.
 * 
 * @return true if it matches the expected data.
 * 
 */
int expect(char read[], int count, char* expected, c_token* tok, FILE* f){
    int xlen = strlen(expected);

    char c;
    tok->content.s = (char*) malloc(sizeof(char) * (xlen + 2));
    assert(tok->content.s != NULL);

    tok->content.s[xlen] = '\0';
    memcpy(tok->content.s, read, count);
    
    int i, ok = 1;
    
    for (i = count; ok && (i < xlen); i++){
        c = fgetc(f);
        if (c == expected[i]){
            tok->content.s[i] = c;
        }
        else{
            tok->content.s[i] = '\0';
            ok = 0;
            ungetc(c, f);
        }
    }
    tok->content.s[i] = '\0';
    tok->content.s[xlen + 1] = '\0';
    
    if (ok){
        if (!is_alphanumeric_under(tok->content.s[xlen] = fgetc(f))){
            ungetc(tok->content.s[xlen], f);
            tok->content.s[xlen] = '\0';
            tok->column_end = tok->column + strlen(tok->content.s);

            return 1;
        }
    }
    return 0;
}


/**
 * Description: Reads a variable token from a file.
 * 
 * @param token The token to save the data in.
 * @param f The file to read data from.
 * 
 */
void read_variable(c_token *token, FILE* f){
    
    int start = 0;
    token->type = VARIABLE;

    if (token->content.s != NULL){
        start = strlen(token->content.s);
    }
    if (!is_alphanumeric_under(token->content.s[start - 1])){
        ungetc(token->content.s[start - 1], f);
        token->content.s[start - 1] = '\0';
    }
    else{
        int size, increments, i;
        
        size = increments = 32;

        token->content.s = (char*) realloc(token->content.s, size);

        for (i = start;;i++){
            if (i >= size){
                size += increments;
                token->content.s = (char*) realloc(token->content.s, size);
            }

            char c = fgetc(f);

            if (is_alphanumeric_under(c)){
                token->content.s[i] = c;
            }
            else{
                ungetc(c, f);
                token->content.s[i] = '\0';
                break;
            }
        }
    }
    token->column_end = token->column + strlen(token->content.s);
}


/**
 * Description: Reads a string until it finds `limit'.
 * 
 * @param f     The file to read from.
 * @param limit The limit character.
 * 
 * @return A string delimited by `limit' (not included).
 * 
 */
char *read_until(FILE *f, char limit){
    int i = 0, size = 0, increments = 32;
    
    int scaped = 0;
    char *s = NULL;
    char c;
    while (!feof(f) && (scaped || ((c = fgetc(f)) != limit))){
        if (i + 2 >= size){ // s[i] = c; s[i + 1] = '\0';
            size += increments;
            s = (char *) realloc(s, size);
            assert(s != NULL);
        }
        if (scaped){
            switch(c){
                case '0':
                    c = '\0';
                    break;

                case 'b':
                    c = '\b';
                    break;
                    
                case 'n':
                    c = '\n';
                    break;
                
                case 'r':
                    c = '\r';
                    break;
                    
                case 't':
                    c = '\t';
                    break;
            }
            s[i] = c;
            i++;

            scaped = 0;
        }
        else if(c != '\\'){
            s[i] = c;
            i++;
        }
        else{
            scaped = 1;
        }
    }
    s[i] = '\0';
    
    return s;
}


/**
 * Description: Reads a token from a file.
 * 
 * @param f File to read tokens from.
 * @param line Line number.
 * @param column Column number.
 * 
 * @return The next c_token.
 * @todo add column_end
 * @todo auto-generate
 */
c_token read_token(FILE* f, int line, int column){
    int c, c1 = fgetc(f), c2, c3, c4;
    int done;

    c_token token;

    token.line = token.line_end = line;
    token.column = column;
    token.column_end = column + 1;
    token.representation = STRING_R;
    token.content.s = NULL;
    token.type = NONE;
    
    // Integer/float parsing
    char tmp;
    
    float value = 0;
    int divisor = 10;
    int passed_dot = 0;


    // Character parsing tree
    switch(c1){
        case EOF:
            token.type = END_OF_FILE;
            break;

        case '#':
            token.representation = CHARACTER_R;
            token.content.c = '#';
            token.type = HASH;
            break;

        case ':':
            token.representation = CHARACTER_R;
            token.content.c = ':';
            token.type = COLON;
            break;
            
        case ';':
            token.representation = CHARACTER_R;
            token.content.c = ';';
            token.type = SEMICOLON;
            break;
            
        case '.':
            token.representation = CHARACTER_R;
            token.content.c = '.';
            token.type = DOT;
            break;
            
        case ',':
            token.representation = CHARACTER_R;
            token.content.c = ',';
            token.type = COMMA;
            break;
            
        case '<':
            token.representation = CHARACTER_R;
            token.content.c = '<';
            token.type = LT;
            break;
        
        case '>':
            token.representation = CHARACTER_R;
            token.content.c = '>';
            token.type = GT;
            break;
            
        case '+':
            token.representation = CHARACTER_R;
            token.content.c = '+';
            token.type = PLUS;
            break;
            
        case '-':
            token.representation = CHARACTER_R;
            token.content.c = '-';
            token.type = MINUS;
            break;
        
        case '*':
            token.representation = CHARACTER_R;
            token.content.c = '*';
            token.type = PRODUCT;
            break;
        
        case '^':
            token.representation = CHARACTER_R;
            token.content.c = '^';
            token.type = BOOL_XOR;
            break;
        
        case '/':
            token.representation = CHARACTER_R;
            c2 = fgetc(f);
            
            // Line comment
            if (c2 == '/'){
                do{
                    c = fgetc(f);
                }while ((c != '\n') && (c != EOF));

                token.content.c = '\n';
                token.type = LINE_FEED;
            }
            // Multiline comment
            else if (c2 == '*'){
                done = 0;
                while (!done){
                    token.column_end++;
                    c = fgetc(f);
                    if (c == '*'){
                        c2 = fgetc(f);
                        if (c2 == '/'){
                            done = 1;
                        }
                        else if (c2 == '\n'){
                            token.line_end++;
                        }
                    }
                    else if (c == '\n'){
                        token.line_end++;
                    }
                }
                
                // Return next token
                return read_token(f, token.line_end, token.column_end);
            }
            // Division
            else{
                ungetc(c2, f);
                token.content.c = '/';
                token.type = DIVISION;
            }
            break;

        case '&':
            c2 = fgetc(f);
            if (c2 == '&'){
                token.type = LOGIC_AND;
                token.representation = STRING_R;
                token.content.s = strdup("&&");
            }
            else{
                ungetc(c2, f);
                token.type = BOOL_AND;
                token.representation = CHARACTER_R;
                token.content.c = '&';
            }
            break;
        
        case '|':
            c2 = fgetc(f);
            if (c2 == '|'){
                token.type = LOGIC_OR;
                token.representation = STRING_R;
                token.content.s = strdup("||");
            }
            else{
                ungetc(c2, f);
                token.type = BOOL_OR;
                token.representation = CHARACTER_R;
                token.content.c = '|';
            }
            break;
        
        case '\\': /// @todo Trigraphs ?
            c2 = fgetc(f);
            if (is_whitespace(c2) && (c2 != '\n')){
                c2 = fgetc(f);
            }
            switch(c2){
                case '\n':
                    token = read_token(f, line + 1, 0);
                    token.line = line;
                    return token;

                default:
                    ungetc(c2, f);
                    token.representation = CHARACTER_R;
                    token.content.c = '\\';
                    token.type = BACKSLASH;
            }
            break;
            
        case '{':
            token.representation = CHARACTER_R;
            token.content.c = '{';
            token.type = OPEN_BRACE;
            break;
        
        case '}':
            token.representation = CHARACTER_R;
            token.content.c = '}';
            token.type = CLOSE_BRACE;
            break;
        
        case '(':
            token.representation = CHARACTER_R;
            token.content.c = '(';
            token.type = OPEN_PARENS;
            break;
        
        case ')':
            token.representation = CHARACTER_R;
            token.content.c = ')';
            token.type = CLOSE_PARENS;
            break;
            
        case '"':
            token.type = STRING;
            token.representation = STRING_R;
            token.content.s = read_until(f, '"');
            break;
            
        case '\'':
            token.type = CHARACTER;
            token.content.s = read_until(f, '\'');
            if ((token.content.s == NULL) || (strlen(token.content.s) != 1)){
                fprintf(stderr,
                        "\x1b[0;91mError: invalid number of characters"
                        " in character: `%s'\x1b[0m\n",
                            token.content.s);
                token.type = NONE;
                free(token.content.s);
                token.content.s = NULL;
            }
            else{
                tmp = token.content.s[0];
                free(token.content.s);
                
                token.representation = CHARACTER_R;
                token.content.c = tmp;
            }
            break;
            
        case '0': case '1': case '2': case '3': case '4': case '5': case '6':
        case '7': case '8': case '9':
            token.representation = INTEGER_R;
            token.type = INTEGER;

            done = 0;
            
            c = c1;
            while(!done){
                switch(c){
                    case '.':
                        passed_dot = 1;
                        token.representation = FLOAT_R;
                        token.type = FLOAT_N;
                        token.column_end++;
                        break;
                        
                    /// @todo 0x.... 0o ... 0b ...
                    case '0': case '1': case '2': case '3': case '4': case '5':
                    case '6': case '7': case '8': case '9':
                        if (!passed_dot){
                            value = (value * 10) + (c - '0');
                        }
                        else{
                            value += (c - '0') / divisor;
                            divisor *= 10;
                        }
                        token.column_end++;
                        break;

                    default:
                        ungetc(c, f);
                        done = 1;
                }
                if (!done){
                    c = fgetc(f);
                }
            }
            if (token.representation == INTEGER_R){
                token.content.i = (int) value;
            }
            else{
                token.content.f = value;
            }
            break;

        case '\n':
            token.representation = CHARACTER_R;
            token.content.c = '\n';
            token.type = LINE_FEED;
            break;

        case 'a': // Tokens starting with a (auto)
            if (expect("a", 1, "auto", &token, f)){
                token.type = AUTO;
            }
            else{
                read_variable(&token, f);
            }

            break;
        
        case 'b': // Tokens starting with b (break)
            if (expect("b", 1, "break", &token, f)){
                token.type = BREAK;
            }
            else{
                read_variable(&token, f);
            }
            
            break;
        
        case 'c': // Tokens starting with c (continue, case, const, char)
            c2 = fgetc(f);
            switch(c2){
                case 'a': // case ?
                    if (expect("ca", 2, "case", &token, f)){
                        token.type = CASE;
                    }
                    else{
                        read_variable(&token, f);
                    }
                    break;
                
                case 'h': // char ?
                    if (expect("ch", 2, "char", &token, f)){
                        token.type = CHAR;
                    }
                    else{
                        read_variable(&token, f);
                    }
                    break;

                case 'o': // const, continue
                
                    c3 = fgetc(f);
                    
                    if (c3 == 'n'){
                        c4 = fgetc(f);
                        switch(c4){
                            case 's': // const
                                if (expect("cons", 4, "const", &token, f)){
                                    token.type = CONST;
                                }
                                else{
                                    read_variable(&token, f);
                                }
                                break;

                            case 't': // continue
                                if (expect("cont", 4, "continue", &token, f)){
                                    token.type = CONTINUE;
                                }
                                else{
                                    read_variable(&token, f);
                                }
                                break;
                                
                            default:
                                token.content.s = (char*) malloc(sizeof(char) * 5);
                                assert(token.content.s != NULL);
                                token.content.s[0] = c1;
                                token.content.s[1] = c2;
                                token.content.s[2] = c3;
                                token.content.s[3] = c4;
                                token.content.s[4] = '\0';
                                
                                read_variable(&token, f);
                                break;
                        }
                    }
                    else{
                        token.content.s = (char*) malloc(sizeof(char) * 4);
                        assert(token.content.s != NULL);
                        token.content.s[0] = c1;
                        token.content.s[1] = c2;
                        token.content.s[2] = c3;
                        token.content.s[3] = '\0';

                        read_variable(&token, f);
                    }
                    break;
                    
                default:
                    token.content.s = (char*) malloc(sizeof(char) * 3);
                    assert(token.content.s != NULL);
                    token.content.s[0] = c1;
                    token.content.s[1] = c2;
                    token.content.s[2] = '\0';

                    read_variable(&token, f);
                    
                    break;
            }

            break;

        case 'd': // Tokens starting with d (define, do, double)
            c2 = fgetc(f);
            switch(c2){
                case 'e':
                    if (expect("de", 2, "define", &token, f)){
                        token.type = DEFINE;
                    }
                    else{
                        read_variable(&token, f);
                    }
                    break;

                case 'o':
                    if (expect("do", 2, "do", &token, f)){
                        token.type = DO;
                    }
                    else{
                        c3 = token.content.s[2];
                        free(token.content.s);
                        token.content.s = NULL;
                        if (c3 == 'u'){
                            if (expect("dou", 3, "double", &token, f)){
                                token.type = DOUBLE;
                            }
                            else{
                                read_variable(&token, f);
                            }
                        }
                        else{
                            token.content.s = (char*) malloc(sizeof(char) * 4);
                            assert(token.content.s != NULL);
                            token.content.s[0] = c1;
                            token.content.s[1] = c2;
                            token.content.s[2] = c3;
                            token.content.s[3] = '\0';

                            read_variable(&token, f);
                        }
                    }
                    
                    break;
                    
                default:
                    token.content.s = (char*) malloc(sizeof(char) * 3);
                    assert(token.content.s != NULL);
                    token.content.s[0] = c1;
                    token.content.s[1] = c2;
                    token.content.s[2] = '\0';

                    read_variable(&token, f);
                    break;
            }
            break;
            
        case 'e': // Tokens starting with e (elif, else, endif, enum, error, extern)
            c2 = fgetc(f);
            switch(c2){
                case 'l': // elif, else
                    c3 = fgetc(f);
                    switch(c3){
                        case 'i': // Elif
                            if (expect("eli", 3, "elif", &token, f)){
                                token.type = ELIF;
                            }
                            else{
                                read_variable(&token, f);
                            }
                            break;
                        
                        case 's': // Else
                            if (expect("els", 3, "else", &token, f)){
                                token.type = ELSE;
                            }
                            else{
                                read_variable(&token, f);
                            }
                            break;
                            
                        default:
                            token.content.s = (char*) malloc(sizeof(char) * 4);
                            assert(token.content.s != NULL);
                            token.content.s[0] = c1;
                            token.content.s[1] = c2;
                            token.content.s[2] = c3;
                            token.content.s[3] = '\0';

                            read_variable(&token, f);
                            break;
                    }
                    break;
                
                case 'n': // Enum
                    c3 = fgetc(f);
                    switch(c3){
                        case 'd':
                            if (expect("end", 3, "endif", &token, f)){
                                token.type = ENDIF;
                            }
                            else{
                                read_variable(&token, f);
                            }
                            break;
                        
                        case 'u':
                            if (expect("enu", 3, "enum", &token, f)){
                                token.type = ENUM;
                            }
                            else{
                                read_variable(&token, f);
                            }
                            break;

                        default:
                            token.content.s = (char*) malloc(sizeof(char) * 4);
                            assert(token.content.s != NULL);
                            token.content.s[0] = c1;
                            token.content.s[1] = c2;
                            token.content.s[2] = c3;
                            token.content.s[3] = '\0';

                            read_variable(&token, f);
                            break;
                    }
                    break;

                case 'r': // Error
                    if (expect("er", 2, "error", &token, f)){
                        token.type = ERROR;
                    }
                    else{
                        read_variable(&token, f);
                    }
                    break;
                    
                case 'x': // Extern
                    if (expect("ex", 2, "extern", &token, f)){
                        token.type = EXTERN;
                    }
                    else{
                        read_variable(&token, f);
                    }
                    break;
                    
                default:
                    token.content.s = (char*) malloc(sizeof(char) * 3);
                    assert(token.content.s != NULL);
                    token.content.s[0] = c1;
                    token.content.s[1] = c2;
                    token.content.s[2] = '\0';

                    read_variable(&token, f);
                    break;
            }
            break;

        case 'f': // Tokens starting with f (for, float) 
            c2 = fgetc(f);
            switch(c2){
                case 'l': // Float
                    if (expect("fl", 2, "float", &token, f)){
                        token.type = FLOAT;
                    }
                    else{
                        read_variable(&token, f);
                    }
                    break;

                case 'o': // For
                    if (expect("fo", 2, "for", &token, f)){
                        token.type = FOR;
                    }
                    else{
                        read_variable(&token, f);
                    }
                    break;

                default:
                    token.content.s = (char*) malloc(sizeof(char) * 3);
                    assert(token.content.s != NULL);
                    token.content.s[0] = c1;
                    token.content.s[1] = c2;
                    token.content.s[2] = '\0';

                    read_variable(&token, f);
                    break;

            }
            break;
        
        case 'g': // Tokens starting with g (goto)
            if (expect("g", 1, "goto", &token, f)){ // Ugh >:P
                token.type = GOTO;
            }
            else{
                read_variable(&token, f);
            }
            break;

        case 'i': // Tokens starting with i (include, if, ifdef, ifnded, int)
            c2 = fgetc(f);
            switch(c2){
                case 'f': // if, ifdef, ifndef
                    if (expect("if", 2, "if", &token, f)){ // If
                    
                        token.type = IF;
                    }
                    else{
                        c3 = token.content.s[2];
                        free(token.content.s);
                        token.content.s = NULL;
                        switch(c3){
                            case 'd': // Ifdef
                                if (expect("ifd", 3, "ifdef", &token, f)){
                                    token.type = IFDEF;
                                }
                                else{
                                    read_variable(&token, f);
                                }
                                break;

                            case 'n': // Ifndef
                                if (expect("ifn", 3, "ifndef", &token, f)){
                                    token.type = IFNDEF;
                                }
                                else{
                                    read_variable(&token, f);
                                }
                                break;
                            
                            default:
                                token.content.s = (char*) malloc(sizeof(char) * 4);
                                assert(token.content.s != NULL);
                                token.content.s[0] = c1;
                                token.content.s[1] = c2;
                                token.content.s[2] = c3;
                                token.content.s[3] = '\0';

                                read_variable(&token, f);
                                break;
                        }
                    }
                    break;

                case 'n': // Int, Include
                    c3 = fgetc(f);
                    switch(c3){
                        case 't': // int
                            if (expect("int", 3, "int", &token, f)){
                                token.type = INT;
                            }
                            else{
                                read_variable(&token, f);
                            }
                            break;
                            
                        case 'c': // include
                            if (expect("inc", 3, "include", &token, f)){
                                token.type = INCLUDE;
                            }
                            else{
                                read_variable(&token, f);
                            }
                            break;

                        default:
                            token.content.s = (char*) malloc(sizeof(char) * 4);
                            assert(token.content.s != NULL);
                            token.content.s[0] = c1;
                            token.content.s[1] = c2;
                            token.content.s[2] = c3;
                            token.content.s[3] = '\0';

                            read_variable(&token, f);
                            break;
                    }
                    break;
                    
                default:
                    token.content.s = (char*) malloc(sizeof(char) * 3);
                    assert(token.content.s != NULL);
                    token.content.s[0] = c1;
                    token.content.s[1] = c2;
                    token.content.s[2] = '\0';

                    read_variable(&token, f);
                    break;
            }
            break;
        
        case 'l': // Tokens starting with l (long)
            if (expect("l", 1, "long", &token, f)){
                token.type = LONG;
            }
            else{
                read_variable(&token, f);
            }
            break;
            
        case 'r': // Tokens starting with r (register, return)
            c2 = fgetc(f);
            if (c2 == 'e'){
                c3 = fgetc(f);
                switch(c3){
                    case 'g': // Register
                        if (expect("reg", 3, "register", &token, f)){
                            token.type = REGISTER;
                        }
                        else{
                            read_variable(&token, f);
                        }
                        break;
                    
                    case 't': // Return
                        if (expect("ret", 3, "return", &token, f)){
                            token.type = RETURN;
                        }
                        else{
                            read_variable(&token, f);
                        }
                        break;
                        
                    default:
                        token.content.s = (char*) malloc(sizeof(char) * 4);
                        assert(token.content.s != NULL);
                        token.content.s[0] = c1;
                        token.content.s[1] = c2;
                        token.content.s[2] = c3;
                        token.content.s[3] = '\0';

                        read_variable(&token, f);
                        break;
                }
            }
            else{
                token.content.s = (char*) malloc(sizeof(char) * 3);
                assert(token.content.s != NULL);
                token.content.s[0] = c1;
                token.content.s[1] = c2;
                token.content.s[2] = '\0';

                read_variable(&token, f);
            }
            break;
            
        case 's': // Tokens starting with s (short, signed, sizeof, struct, static, switch)
            c2 = fgetc(f);
            switch(c2){
                case 'h': // Short
                    if (expect("sh", 2, "short", &token, f)){
                        token.type = SHORT;
                    }
                    else{
                        read_variable(&token, f);
                    }
                    break;

                case 'i': // Signed, sizeof
                    c3 = fgetc(f);
                    switch(c3){
                        case 'g': // Signed
                            if (expect("sig", 3, "signed", &token, f)){
                                token.type = SIGNED;
                            }
                            else{
                                read_variable(&token, f);
                            }
                            break;
                        
                        case 'z': // sizeof
                            if (expect("siz", 3, "sizeof", &token, f)){
                                token.type = SIZEOF;
                            }
                            else{
                                read_variable(&token, f);
                            }
                            break;
                        
                        default:
                            token.content.s = (char*) malloc(sizeof(char) * 4);
                            assert(token.content.s != NULL);
                            token.content.s[0] = c1;
                            token.content.s[1] = c2;
                            token.content.s[2] = c3;
                            token.content.s[3] = '\0';

                            read_variable(&token, f);
                            break;
                    }
                    break;
                    
                case 't': // Static, Struct
                    c3 = fgetc(f);
                    switch(c3){
                        case 'a': // Static
                            if (expect("sta", 3, "static", &token, f)){
                                token.type = STATIC;
                            }
                            else{
                                read_variable(&token, f);
                            }
                            break;
                        
                        case 'r': // Struct
                            if (expect("str", 3, "struct", &token, f)){
                                token.type = STRUCT;
                            }
                            else{
                                read_variable(&token, f);
                            }
                            break;
                        
                        default:
                            token.content.s = (char*) malloc(sizeof(char) * 4);
                            assert(token.content.s != NULL);
                            token.content.s[0] = c1;
                            token.content.s[1] = c2;
                            token.content.s[2] = c3;
                            token.content.s[3] = '\0';

                            read_variable(&token, f);
                            break;
                    }
                    break;
                    
                case 'w':
                    if (expect("sw", 2, "switch", &token, f)){
                        token.type = SWITCH;
                    }
                    else{
                        read_variable(&token, f);
                    }
                    break;
                    
                default:
                    token.content.s = (char*) malloc(sizeof(char) * 3);
                    assert(token.content.s != NULL);
                    token.content.s[0] = c1;
                    token.content.s[1] = c2;
                    token.content.s[2] = '\0';

                    read_variable(&token, f);
                    break;
            }
            break;

        case 't': // Tokens starting with t (typedef)
            if (expect("t", 1, "typedef", &token, f)){
                token.type = TYPEDEF;
            }
            else{
                read_variable(&token, f);
            }
            break;
            
        case 'u': // Tokens starting with u (undef, union, unsigned)
            c2 = fgetc(f);
            if (c2 == 'n'){
                c3 = fgetc(f);
                switch(c3){
                    case 'd': // undef
                        if (expect("und", 3, "undef", &token, f)){
                            token.type = UNDEFINE;
                        }
                        else{
                            read_variable(&token, f);
                        }
                        break;
                        
                    case 'i': // union
                        if (expect("uni", 3, "union", &token, f)){
                            token.type = UNION;
                        }
                        else{
                            read_variable(&token, f);
                        }
                        break;
                    
                    case 's': // unsigned
                        if (expect("uns", 3, "unsigned", &token, f)){
                            token.type = UNSIGNED;
                        }
                        else{
                            read_variable(&token, f);
                        }
                        break;
                        
                    default:
                        token.content.s = (char*) malloc(sizeof(char) * 4);
                        assert(token.content.s != NULL);
                        token.content.s[0] = c1;
                        token.content.s[1] = c2;
                        token.content.s[2] = c3;
                        token.content.s[3] = '\0';

                        read_variable(&token, f);
                        break;
                }
            }
            else{
                token.content.s = (char*) malloc(sizeof(char) * 3);
                assert(token.content.s != NULL);
                token.content.s[0] = c1;
                token.content.s[1] = c2;
                token.content.s[2] = '\0';

                read_variable(&token, f);
            }
            break;
        
        case 'v': // Tokens starting with 'v' (void, volatile)
            c2 = fgetc(f);
            if (c2 == 'o'){
                c3 = fgetc(f);
                switch(c3){
                    case 'i':
                        if (expect("voi", 3, "void", &token, f)){
                            token.type = VOID;
                        }
                        else{
                            read_variable(&token, f);
                        }
                        break;
                    
                    case 'l':
                        if (expect("vol", 3, "volatile", &token, f)){
                            token.type = VOLATILE;
                        }
                        else{
                            read_variable(&token, f);
                        }
                        break;

                    default:
                        token.content.s = (char*) malloc(sizeof(char) * 4);
                        assert(token.content.s != NULL);
                        token.content.s[0] = c1;
                        token.content.s[1] = c2;
                        token.content.s[2] = c3;
                        token.content.s[3] = '\0';
                        
                        read_variable(&token, f);
                        break;
                }
            }
            else{
                token.content.s = (char*) malloc(sizeof(char) * 3);
                assert(token.content.s != NULL);
                token.content.s[0] = c1;
                token.content.s[1] = c2;
                token.content.s[2] = '\0';
                
                read_variable(&token, f);
            }
            break;

        case 'w': // Tokens starting with w (warning, while)
            c2 = fgetc(f);
            switch(c2){
                case 'a':
                    if (expect("wa", 2, "warning", &token, f)){
                        token.type = WARNING;
                    }
                    else{
                        read_variable(&token, f);
                    }
                    break;
                
                case 'h':
                    if (expect("wh", 2, "while", &token, f)){
                        token.type = WHILE;
                    }
                    else{
                        read_variable(&token, f);
                    }
                    break;
                    
                default:
                    token.content.s = (char*) malloc(sizeof(char) * 3);
                    assert(token.content.s != NULL);
                    token.content.s[0] = c1;
                    token.content.s[1] = c2;
                    token.content.s[2] = '\0';
                    
                    read_variable(&token, f);
                    break;
            }    
            break;
                
        default:
            if (is_alphanumeric_under(c1)){
                token.content.s = (char*) malloc(sizeof(char) * 2);
                assert(token.content.s != NULL);
                token.content.s[0] = c1;
                token.content.s[1] = '\0';
                read_variable(&token, f);
            }
            else{
                token.type = NONE;
                token.representation = CHARACTER_R;
                token.content.c = c1;
            }
            break;
    }
    
    return token;
}


/**
 * Description: Reads a token line from a file. Must free it after use.
 * 
 * @param f File to read tokens from.
 * @param line Line number.
 * 
 * @return A linked list of c_tokens.
 * 
 */
llist_t* read_token_line(FILE* f, int *line, int col){
    llist_t *tok_list = create_llist();
    assert(tok_list != NULL);

    while(1){
        char c = fgetc(f);
        while (is_whitespace(c) && (c != '\n')){
            c = fgetc(f);
            col++;
        }
        ungetc(c, f);

        c_token t = read_token(f, *line, col);
        *line = t.line_end;
        if ((t.type == LINE_FEED) || (t.type == END_OF_FILE)){
            *line--;
            break;
        }
        else{
            if (!((t.type == VARIABLE) && (t.content.s == NULL))){
                add_to_llist(tok_list, copy_token(t));
            }
            free_token(t);
        }
        col = t.column_end + 1;
    }

    return tok_list;
}

#endif
