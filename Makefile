CC=gcc
CFLAGS=-ggdb -Wall -Wno-write-strings -Werror=parentheses
OBJ_DIR=obj
BIN_DIR=bin
HEADER_DIR=headers
TEST_DIR=test
TESTOBJ_DIR=$(TEST_DIR)/obj
TESTHEADER_DIR=$(TEST_DIR)/headers

# Object files
CORETYPE_OBJS=$(OBJ_DIR)/hash_table.o $(OBJ_DIR)/linked_list.o $(OBJ_DIR)/binary_tree.o
CORE_OBJS=$(CORETYPE_OBJS) $(OBJ_DIR)/bencode.o $(OBJ_DIR)/bytecode_manager.o

all: dirs library parser_lib

####################################################################
# Generate documentation
####################################################################

docs:
	doxygen doxygen_doc.cfg

####################################################################
# Generic library
####################################################################
library: core_lib $(BIN_DIR)/interpreter

#-------------------------------------------------------------------
# Interpreter
#-------------------------------------------------------------------
$(OBJ_DIR)/interpreter.o: lib/interpreter/interpreter.c
	$(CC) lib/interpreter/interpreter.c -c -o $@ -I$(HEADER_DIR) $(CFLAGS)

$(OBJ_DIR)/basic_interpreter.o: lib/interpreter/basic_interpreter.c
	$(CC) lib/interpreter/basic_interpreter.c -c -o $@ -I$(HEADER_DIR) $(CFLAGS)

$(BIN_DIR)/interpreter: $(OBJ_DIR)/interpreter.o $(OBJ_DIR)/basic_interpreter.o $(CORE_OBJS)
	$(CC) $(OBJ_DIR)/interpreter.o $(OBJ_DIR)/basic_interpreter.o $(CORE_OBJS) -o $@ $(CFLAGS) 

#-------------------------------------------------------------------
# Core
#-------------------------------------------------------------------
core_lib: $(CORE_OBJS)

$(OBJ_DIR)/hash_table.o: lib/core/types/hash_table/hash_table.c
	$(CC) lib/core/types/hash_table/hash_table.c -c -o $@ $(CFLAGS) -I$(HEADER_DIR)

$(OBJ_DIR)/linked_list.o: lib/core/types/linked_list/linked_list.c
	$(CC) lib/core/types/linked_list/linked_list.c -c -o $@ $(CFLAGS) -I$(HEADER_DIR)

$(OBJ_DIR)/binary_tree.o: lib/core/types/binary_tree/binary_tree.c
	$(CC) lib/core/types/binary_tree/binary_tree.c -c -o $@ $(CFLAGS) -I$(HEADER_DIR)

$(OBJ_DIR)/bencode.o: $(CORETYPE_OBJS) lib/core/bencode/*.c
	$(CC) lib/core/bencode/bencode.c -c -o $@ $(CFLAGS) -I$(HEADER_DIR)

$(OBJ_DIR)/bytecode_manager.o: $(CORETYPE_OBJS) $(OBJ_DIR)/bencode.o lib/core/bytecode_manager/bytecode_manager.c
	$(CC) lib/core/bytecode_manager/bytecode_manager.c -c -o $@ $(CFLAGS) -I$(HEADER_DIR)


####################################################################
# Tests
####################################################################
$(TESTOBJ_DIR)/linked_list.o: $(TEST_DIR)/lib/core/types/linked_list/linked_list_tests.c
	$(CC) $(TEST_DIR)/lib/core/types/linked_list/linked_list_tests.c -c -o $@ -I$(HEADER_DIR) -lcunit $(CFLAGS)

$(TESTOBJ_DIR)/binary_tree.o: $(TEST_DIR)/lib/core/types/binary_tree/binary_tree_tests.c
	$(CC) $(TEST_DIR)/lib/core/types/binary_tree/binary_tree_tests.c -c -o $@ -I$(HEADER_DIR) -lcunit $(CFLAGS)

$(TESTOBJ_DIR)/hash_table.o: $(TEST_DIR)/lib/core/types/hash_table/hash_table_tests.c
	$(CC) $(TEST_DIR)/lib/core/types/hash_table/hash_table_tests.c -c -o $@ -I$(HEADER_DIR) -lcunit $(CFLAGS)

$(TESTOBJ_DIR)/c_tokenizer_test.o: $(OBJ_DIR)/c_tokens.o $(TEST_DIR)/parsers/c/c_tokenizer_tests.c
	$(CC) $(TEST_DIR)/parsers/c/c_tokenizer_tests.c -c -o $@ -I$(HEADER_DIR) -lcunit $(CFLAGS)

$(TESTOBJ_DIR)/tests.o:  $(TESTOBJ_DIR)/binary_tree.o $(TESTOBJ_DIR)/linked_list.o $(TESTOBJ_DIR)/hash_table.o $(TESTOBJ_DIR)/c_tokenizer_test.o $(TEST_DIR)/tests.c
	$(CC) $(TEST_DIR)/tests.c -c -o $@ -I$(TESTHEADER_DIR) $(CFLAGS)

$(TEST_DIR)/tests: $(TESTOBJ_DIR)/tests.o
	$(CC) $(TESTOBJ_DIR)/*.o $(CORE_OBJS) $(OBJ_DIR)/c_tokens.o -o $@ $(CFLAGS) -lcunit

test_dirs:
	mkdir $(TEST_DIR)/obj || true

tests: all test_dirs $(TEST_DIR)/tests
	$(TEST_DIR)/tests

####################################################################
# Parsing library
####################################################################
parser_lib: c_parser_lib

# C parser
# C tokenizer
$(OBJ_DIR)/c_tokens.o: parsers/c/c_tokens.c
	$(CC) parsers/c/c_tokens.c -c -o $@ -I$(HEADER_DIR) $(CFLAGS)

# C preprocessor
$(OBJ_DIR)/standalone_c_preprocessor.o: parsers/c/c_preprocessor.c
	$(CC) parsers/c/c_preprocessor.c -c -o $@ -DSTANDALONE -I$(HEADER_DIR) $(CFLAGS)

$(OBJ_DIR)/c_preprocessor.o: parsers/c/c_preprocessor.c
	$(CC) parsers/c/c_preprocessor.c -c -o $@ -I$(HEADER_DIR) $(CFLAGS)

$(BIN_DIR)/c_preprocessor: $(CORETYPE_OBJS) $(OBJ_DIR)/c_tokens.o $(OBJ_DIR)/standalone_c_preprocessor.o
	$(CC) $(CORETYPE_OBJS) $(OBJ_DIR)/c_tokens.o $(OBJ_DIR)/standalone_c_preprocessor.o \
					 -o $@ -I$(HEADER_DIR) $(CFLAGS)

# C parser
$(OBJ_DIR)/c_parser.o: parsers/c/c_parser.c
	$(CC) parsers/c/c_parser.c -c -o $@ -I$(HEADER_DIR) $(CFLAGS)

$(BIN_DIR)/c_parser: $(CORETYPE_OBJS) $(OBJ_DIR)/c_tokens.o $(OBJ_DIR)/c_preprocessor.o $(OBJ_DIR)/c_parser.o
	$(CC) $(CORETYPE_OBJS) $(OBJ_DIR)/c_tokens.o $(OBJ_DIR)/c_preprocessor.o $(OBJ_DIR)/c_parser.o \
						-o $@ $(CFLAGS)

c_parser_lib: $(BIN_DIR)/c_preprocessor $(BIN_DIR)/c_parser


####################################################################
# Misc.
####################################################################
dirs:
	mkdir bin || true
	mkdir obj || true

clean_tests:
	rm -Rf $(TEST_DIR)/tests  $(TESTOBJ_DIR)

clean: clean_tests
	rm -Rf $(BIN_DIR) $(OBJ_DIR)
