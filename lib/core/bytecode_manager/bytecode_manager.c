#ifndef BYTECODE_MANAGER_C
#define BYTECODE_MANAGER_C

#include "bytecode_manager.h"

/**
 * @file bytecode_manager.c
 *
 * @brief Bytecode data manager.
 * 
 * @todo Remove bencode structure dependencies.
 * 
 */

/* Internal constants */
char *entry_point_string = "entry";
char *function_tree_string = "functions";
char *code_string = "code";

char *op_code_string   = "op";
char *op_group_string  = "group";
char *op_params_string = "params";
char *op_return_string = "dest";
char *op_function_name_string = "op";
char *op_condition_string = "condition";

char *op_branch_type_string = "branch_type";
char *op_branch_eval_code_string = "eval_ops";
char *op_branch_step_code_string = "step_ops";
char *op_branch_code_string = "code";

char *var_name_string = "name";
char *var_type_string = "type";
char *var_pos_string  = "pos";

char *type_dim_string = "structure";

char *variable_frame_string = "variables";
char *function_frame_string = "functions";
char *function_params_string = "params";


/**
 * Description: Create a new bytecode manager descriptor.
 * 
 * @return The bytecode manager descriptor.
 * 
 */
bytecode_manager_t *create_bytecode_manager(){
    bytecode_manager_t *bm = (bytecode_manager_t *) 
                             malloc(sizeof(bytecode_manager_t));


    if (bm == NULL){
        return NULL;
    }
    
    bm->tree = create_bencoded_dictionary();

    return bm;
}


/**
 * Description: Create a new bytecode manager descriptor.
 * 
 * @param f The bytecode file.
 * 
 * @return The bytecode manager descriptor.
 * 
 */
bytecode_manager_t *create_bytecode_manager_from_file(FILE *f){
    bytecode_manager_t *bm = (bytecode_manager_t *) 
                             malloc(sizeof(bytecode_manager_t));

    if (bm == NULL){
        perror("malloc");
        return NULL;
    }
    
    /* Read lines until the first character is not a '#' */
    int c;
    c = fgetc(f);
    while (c == '#'){
        do{
            c = fgetc(f);
        }while (c != '\n');
        
        c = fgetc(f);
    }

    fseek(f, ftell(f) - 1, SEEK_SET);    /* Return one byte back */
    
    bm->tree = read_bencoded(f);
    if (bm->tree == NULL){
        free(bm);
        return NULL;
    }

    return bm;
}


/**
 * Description: Create a new bytecode manager descriptor.
 * 
 * @param fname The bytecode file name.
 * 
 * @return The bytecode manager descriptor.
 * 
 */
bytecode_manager_t *create_bytecode_manager_from_file_name(char *fname){
    FILE *f = fopen(fname, "rb");

    if (f == NULL){
        return NULL;
    }
    bytecode_manager_t *bm = create_bytecode_manager(f);
    fclose(f);
    
    return bm;
}


/**
 * Description: Returns the root frame of a bytecode manager descriptor.
 * 
 * @param bm The bytecode manager descriptor.
 * 
 * @return The root frame.
 * 
 */
bencoded_t *get_root_frame(bytecode_manager_t *bm){
    if (bm == NULL){
        return NULL;
    }
    
    return bm->tree;
}


/**
 * Description: Adds a function to the frame
 * 
 * @param bm The bytecode manager descriptor.
 * @param function_name The name of the function name.
 * @param function_type The type of the function.
 * @param function_parameters The parameters of the function.
 * @param frame The frame where the function is going to be inserted.
 * 
 * @return The inserted function frame.
 * 
 */
bencoded_t * add_function(bytecode_manager_t *bm,
                  char * function_name,
                  bencoded_t *function_type,
                  bencoded_t *function_parameters,
                  bencoded_t *frame){

    
    bencoded_t *function_frame = (bencoded_t *) get_hash_table(
                                                     (hash_table_t)frame->v.p,
                                                     "functions");

    if (function_frame == NULL){
        function_frame = create_bencoded_dictionary();
        insert_hash_table((hash_table_t)frame->v.p,
                          "functions", function_frame);
    }

    bencoded_t *function = create_bencoded_dictionary();
    hash_table_t table = (hash_table_t)function->v.p;

    insert_hash_table((hash_table_t)function_frame->v.p,
                      function_name,
                      function);
    
    
    bencoded_t *subfunction_frame = create_bencoded_dictionary();
    bencoded_t *variable_frame = create_bencoded_dictionary();
    
    insert_hash_table(table, "name", function_name);
    insert_hash_table(table, "type", function_type);
    insert_hash_table(table, "functions", subfunction_frame);
    insert_hash_table(table, "variables", variable_frame);
    insert_hash_table(table, "params", function_parameters);
    
    bencoded_t *is_extern = (bencoded_t *) get_hash_table(
                                               (hash_table_t)function_type->v.p,
                                                "extern");

    if (is_extern != NULL){
        if (! is_extern){
            bencoded_t * code_frame = create_bencoded_list();
            insert_hash_table(table, "code", code_frame);
            
            /**
             * @todo add parameters to variable frame.
             *
             * for p in parameters:
             *  if 'name' in p:
             *      self.add_variable(p['name'], p, r[fid], "function parameter")
             *
             *  else:
             *      raise Exception("Unnamed parameter %s" % p)
             *
             */
        }
    }
    
    return function;
}


/**
 * Description: Adds a branch to the frame code.
 * 
 * @param bm The bytecode manager.
 * @param branch_type The branch type code.
 * @param condition The branching condition.
 * @param frame The code frame.
 * @param eval_ops Operations to evaluate before condition checking.
 * @param step_ops Operations to evaluate between steps.
 * 
 * @return The bencoded brach frame.
 * 
 */
bencoded_t * add_branch(bytecode_manager_t *bm,
                int branch_type,
                bencoded_t *condition,
                bencoded_t *frame,
                bencoded_t *eval_ops,
                bencoded_t *step_ops){

    bencoded_t *b = create_bencoded_dictionary();

    assert(b != NULL);

    hash_table_t t = (hash_table_t) b->v.p;
    
    llist_t *code_frame = (llist_t *)
                          get_hash_table((hash_table_t) frame->v.p, "code");


    insert_hash_table(t, "group",       create_bencoded_integer(BRANCH_GROUP));
    insert_hash_table(t, "branch_type", create_bencoded_integer(branch_type));

    insert_hash_table(t, "condition",   condition);
    insert_hash_table(t, "eval_ops",    eval_ops);
    insert_hash_table(t, "step_ops",    step_ops);

    insert_hash_table(t, "code",        create_bencoded_list());

    insert_hash_table(t, "functions",   create_bencoded_dictionary());
    insert_hash_table(t, "variables",   create_bencoded_dictionary());

    
    add_to_llist(code_frame, b);

    return b;
}


/**
 * Description: Obtains the bytecode entry point.
 * 
 * @param bm Code associated bytecode manager.
 * 
 * @return The entry point function bencoded tree.
 * 
 */
bencoded_t * get_entry_point(bytecode_manager_t *bm){
    assert(bm != NULL);
    assert(bm->tree != NULL);

    /* Retrieve entry string struct */
    bencoded_t *entry_str_b = (bencoded_t *) get_hash_table(
                                                    (hash_table_t)bm->tree->v.p,
                                                            entry_point_string);

    if (entry_str_b == NULL){
        return NULL;
    }

    /* Convert to string */
    assert(entry_str_b->type == STR_TYPE);
    char *entry_str = get_bencoded_string(entry_str_b);

    if (entry_str == NULL){
        return NULL;
    }

    /* Retrieve function tree */
    bencoded_t *functions = (bencoded_t *) get_hash_table(
                                                    (hash_table_t)bm->tree->v.p,
                                                          function_tree_string);

    if (functions == NULL){
        return NULL;
    }
    assert(functions->type == DICT_TYPE);

    /* Return the entry function */
    return (bencoded_t *) 
            get_hash_table((hash_table_t) functions->v.p, entry_str);
}


/**
 * Description: Returns the operation list from a frame.
 * 
 * @param frame The frame containing the code.
 * 
 * @return The code operations linked list.
 * 
 */
llist_t *get_code_from_frame(bencoded_t *frame){
    bencoded_t *b = (bencoded_t *) get_hash_table((hash_table_t) frame->v.p,
                                                  code_string);
    
    if ((b == NULL) || (b->type != LIST_TYPE)){
        return NULL;
    }
    
    return get_bencoded_llist(b);
}


/**
 * Description: Extracts the operation group from a bencoded operation.
 * 
 * @param op The operation.
 * 
 * @return The group code of the operation.
 * 
 */
int get_op_group(bencoded_t *op){
    if (op == NULL){
        return NO_GROUP;
    }
    if (op->type != DICT_TYPE){
        return NO_GROUP;
    }
    
    hash_table_t t = (hash_table_t) op->v.p;
    if (t == NULL){
        return NO_GROUP;
    }
    
    bencoded_t *group_b = (bencoded_t *) get_hash_table(t, op_group_string);
    if (group_b == NULL){
        return NO_GROUP;
    }
    
    if (group_b->type != INT_TYPE){
        return NO_GROUP;
    }
    
    return get_bencoded_integer(group_b);    
}


/**
 * Description: Extracts the operation code from a bencoded operation.
 * 
 * @param op The operation.
 * 
 * @return The operation code of the operation.
 * 
 */
bencoded_t *get_op_code(bencoded_t *op){
    hash_table_t t = get_bencoded_dictionary(op);
    if (t == NULL){
        return NULL;
    }
    
    return (bencoded_t *)get_hash_table(t, op_code_string);     
}


/**
 * Description: Returns a list of the operation parameters.
 * 
 * @param op The operation.
 * 
 * @return A linked list contaning the operation parameters.
 * 
 */
bencoded_t *get_op_params(bencoded_t *op){
    assert(op->type == DICT_TYPE);
    hash_table_t t = get_bencoded_dictionary(op);
    if (t == NULL){
        return NULL;
    }
    
    bencoded_t *b = (bencoded_t *)get_hash_table(t, op_params_string);
    return b;        
}


/**
 * Description: Obtains the variable where to store the results of 
 *  the operation.
 * 
 * @param op The operation.
 * 
 * @return A bencoded variable reference.
 * 
 */
bencoded_t *get_op_return_var(bencoded_t *op){
    assert(op->type == DICT_TYPE);
    hash_table_t t = get_bencoded_dictionary(op);
    if (t == NULL){
        return NULL;
    }
    
    bencoded_t *b = (bencoded_t *)get_hash_table(t, op_return_string);
    return b;        
}


/**
 * Description: Returns the operation function name.
 * 
 * @param op The operation.
 * 
 * @return The function name.
 * 
 */
char *get_op_function_name(bencoded_t *op){
    assert(op->type == DICT_TYPE);
    hash_table_t t = get_bencoded_dictionary(op);
    if (t == NULL){
        return NULL;
    }
    
    bencoded_t *b = (bencoded_t *)get_hash_table(t, op_function_name_string);
    if (b == NULL){
        return NULL;
    }
    if (b->type != STR_TYPE){
        return NULL;
    }
    
    return get_bencoded_string(b);
}


/**
 * Description: Returns the branching condition of a operation.
 * 
 * @param op The branching operation where the condition is evaluated.
 * 
 * @return The condition variable.
 * 
 */
bencoded_t *get_op_condition(bencoded_t *op){
    assert(op->type == DICT_TYPE);
    hash_table_t t = get_bencoded_dictionary(op);
    if (t == NULL){
        return NULL;
    }
    
    bencoded_t *b = (bencoded_t *)get_hash_table(t, op_condition_string);
    return b;        
}


/**
 * Description: Returns the branch type of a operation.
 * 
 * @param op The bencoded operation.
 * 
 * @return It's branch type.
 * 
 */
int get_branch_type(bencoded_t *op){
    assert(op->type == DICT_TYPE);
    hash_table_t t = get_bencoded_dictionary(op);
    assert(t != NULL);
    
    bencoded_t *b = (bencoded_t *)get_hash_table(t, op_branch_type_string);
    assert(b != NULL);
    
    return get_bencoded_integer(b);
}


/**
 * Description: Returns the operations to take before evaluating
 *  the branching condition.
 * 
 * @param branch The bencoded branching operation.
 * 
 * @return The operations to take.
 * 
 */
llist_t* get_branch_eval_code(bencoded_t *branch){
    assert(branch->type == DICT_TYPE);
    hash_table_t t = get_bencoded_dictionary(branch);
    assert(t != NULL);
    
    bencoded_t *b = (bencoded_t *)get_hash_table(t, op_branch_eval_code_string);
    assert(b != NULL);
    
    return get_bencoded_llist(b);
}


/**
 * Description: Returns the operations to take when evaluating
 *  the branching condition only after the first iteration.
 * 
 * @param branch The bencoded branching operation.
 * 
 * @return The operations to take.
 * 
 */
llist_t* get_branch_step_code(bencoded_t *branch){
    assert(branch->type == DICT_TYPE);
    hash_table_t t = get_bencoded_dictionary(branch);
    assert(t != NULL);
    
    bencoded_t *b = (bencoded_t *)get_hash_table(t, op_branch_step_code_string);
    assert(b != NULL);
    
    return get_bencoded_llist(b);
}


/**
 * Description: Returns the operations to take inside a loop or conditional.
 * 
 * @param branch The bencoded branching operation.
 * 
 * @return The operations to take.
 * 
 */
llist_t* get_branch_code(bencoded_t *branch){
    assert(branch->type == DICT_TYPE);
    hash_table_t t = get_bencoded_dictionary(branch);
    assert(t != NULL);
    
    bencoded_t *b = (bencoded_t *)get_hash_table(t, op_branch_code_string);
    assert(b != NULL);
    
    return get_bencoded_llist(b);
}


/**
 * Description: Returns the dimension list of a variable.
 * 
 * @param var The variable to look in.
 * 
 * @return A linked list containing the secuence of sizes.
 * 
 */
llist_t *get_var_dim(bencoded_t *var){
    assert(var->type == DICT_TYPE);
    hash_table_t t = get_bencoded_dictionary(var);
    
    bencoded_t *type_b = (bencoded_t *)get_hash_table(t, var_type_string);
    if (type_b == NULL){
        return NULL;
    }
    assert(type_b->type == DICT_TYPE);
    
    hash_table_t type_table = get_bencoded_dictionary(type_b);
    assert(type_table != NULL);
    
    bencoded_t *pos_b = (bencoded_t *)get_hash_table(type_table,
                                                     type_dim_string);

    if (pos_b == NULL){
        return NULL;
    }
    assert(pos_b->type == LIST_TYPE);
    
    return get_bencoded_llist(pos_b);
}


/**
 * Description: Returns the consecuent array location list of a variable
 *   reference.
 * 
 * @param var The variable to look in.
 * 
 * @return A linked list containing the secuence of references.
 * 
 */
llist_t *get_var_pos(bencoded_t *var){
    assert(var->type == DICT_TYPE);
    hash_table_t t = get_bencoded_dictionary(var);

    bencoded_t *pos_b = (bencoded_t *)get_hash_table(t,
                                                     var_pos_string);

    if (pos_b == NULL){
        return NULL;
    }
    assert(pos_b->type == LIST_TYPE);
    
    return get_bencoded_llist(pos_b);
}


/**
 * Description: Obtains the name of a bencoded variable.
 * 
 * @param var The variable to obtain the name from.
 * 
 * @return The name of the variable or NULL if not found.
 * 
 */
char *get_var_name(bencoded_t *var){
    assert(var->type == DICT_TYPE);
    hash_table_t t = get_bencoded_dictionary(var);
    
    bencoded_t *b = (bencoded_t *)get_hash_table(t, var_name_string);
    if (b == NULL){
        return NULL;
    }
    assert(b->type == STR_TYPE);
    
    return get_bencoded_string(b);
}


/**
 * Description: Returns either or not a frame contains a variable.
 * 
 * @param var_name The variable name.
 * @param frame    The code frame.
 * 
 * @return 0 if it doesn't contains the variable and 1 if it does.
 * 
 */
int frame_has_variable(char *var_name, bencoded_t *frame){
    assert(frame != NULL);
    assert(frame->type == DICT_TYPE);
    
    hash_table_t t = get_bencoded_dictionary(frame);
    bencoded_t *variable_frame_b = (bencoded_t *) 
                                    get_hash_table(t, variable_frame_string);

 
    assert(variable_frame_b != NULL);
    assert(variable_frame_b->type == DICT_TYPE);
    
    hash_table_t var_table = get_bencoded_dictionary(variable_frame_b);
    assert(var_table != NULL);

    return (get_hash_table(var_table, var_name) != NULL);
}


/**
 * Description: Looks for a function in a frame.
 * 
 * @param fun_name The function name.
 * @param frame    The code frame.
 * 
 * @return The bencoded function or NULL if is not found.
 * 
 */
bencoded_t *get_frame_function(char *fun_name, bencoded_t *frame){
    assert(frame != NULL);
    assert(frame->type == DICT_TYPE);
    
    hash_table_t t = get_bencoded_dictionary(frame);
    bencoded_t *function_frame_b = (bencoded_t *) 
                                    get_hash_table(t, function_frame_string);

 
    assert(function_frame_b != NULL);
    assert(function_frame_b->type == DICT_TYPE);
    
    hash_table_t fun_table = get_bencoded_dictionary(function_frame_b);
    assert(fun_table != NULL);

    return (bencoded_t *) get_hash_table(fun_table, fun_name);
}


/**
 * Description: Returns a list of the variable names in the frame.
 * 
 * @param frame    The code frame.
 * 
 * @return The string list of variable names.
 * 
 */
llist_t *get_frame_var_names(bencoded_t *frame){
    assert(frame != NULL);
    assert(frame->type == DICT_TYPE);
    
    hash_table_t t = get_bencoded_dictionary(frame);
    bencoded_t *variable_frame_b = (bencoded_t *) 
                                    get_hash_table(t, variable_frame_string);

    
    assert(variable_frame_b != NULL);
    assert(variable_frame_b->type == DICT_TYPE);
    
    hash_table_t var_table = get_bencoded_dictionary(variable_frame_b);
    assert(var_table != NULL);

    return get_hash_table_str_keys(var_table);
}

/**
 * Description: Returns a list of the function parameters.
 * 
 * @param function The function frame.
 * 
 * @return The variable list of parameters.
 * 
 */
llist_t *get_function_params(bencoded_t *function){
    assert(function != NULL);

    hash_table_t ht = get_bencoded_dictionary(function);
    bencoded_t *blist = (bencoded_t *) 
                                    get_hash_table(ht, function_params_string);

    assert(blist != NULL);
    assert(blist->type == LIST_TYPE);
    
    llist_t* param_list = get_bencoded_llist(blist);
    return param_list;
}


/**
 * Description: Returns a variable object from a code frame.
 * 
 * @param frame The code frame.
 * @param var_name The variable name.
 * 
 * @return The bencoded variable.
 * 
 */
bencoded_t *get_var(bencoded_t *frame, char *var_name){
    assert(frame != NULL);
    assert(frame->type == DICT_TYPE);
    
    hash_table_t t = get_bencoded_dictionary(frame);
    bencoded_t *variable_frame_b = (bencoded_t *) 
                                    get_hash_table(t, variable_frame_string);
    
    assert(variable_frame_b != NULL);
    assert(variable_frame_b->type == DICT_TYPE);
    
    hash_table_t var_table = get_bencoded_dictionary(variable_frame_b);
    assert(var_table != NULL);

    return ((bencoded_t *) get_hash_table(var_table, var_name));
}


/**
 * Describe: Checks if a bencoded variable points to some actual one.
 * 
 * @param var The variable to check.
 * 
 * @return 0 if it's a void value, 1 elsewhere.
 * 
 */
int is_meaningfull_variable(bencoded_t *var){
    int meaningfull = 0;
    switch(var->type){
        case STR_TYPE:
            meaningfull = strlen(get_bencoded_string(var)) > 0;
            break;
            
        case DICT_TYPE:
            meaningfull = (get_hash_table_str_keys(get_bencoded_dictionary(var))
                                                                    ->size) > 0;
            break;
            
        /** @todo Check this, this is a strange returning variable type */
        /* case INT_TYPE: */
        /* case LIST_TYPE: */
        default:
            break;
    }
    return meaningfull;
}

/**
 * Description: Frees the bytecode manager memory.
 * 
 * @param bm The bytecode manager to free.
 * 
 */
void free_bytecode_manager(bytecode_manager_t *bm){

    if (bm != NULL){
        if (bm->tree != NULL){
            free_bencoded(bm->tree);
        }

        free(bm);
    }
 }

#endif
