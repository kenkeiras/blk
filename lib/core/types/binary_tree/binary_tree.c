#ifndef BINARY_TREE_C
#define BINARY_TREE_C

#include "binary_tree.h"

/**
 * @file binary_tree.c
 *
 * @brief A binary search tree.
 */

struct binary_tree_node {
    void *content;
    char *key;

    struct binary_tree_node *parent;
    struct binary_tree_node *right;
    struct binary_tree_node *left;
};

struct binary_tree {
    struct binary_tree_node *top;
};


/**
 * Description: Creates a binary tree.
 *
 * @return A void binary tree.
 *
 */
binary_tree_t *create_binary_tree(){
    binary_tree_t *tree = (binary_tree_t *) malloc(sizeof(binary_tree_t));
    if (tree != NULL){
	tree->top = NULL;
    }

    return tree;
}


binary_tree_node_t *make_binary_tree_node(char *key, void *content){
    binary_tree_node_t *node = (binary_tree_node_t *) 
                               malloc(sizeof(binary_tree_node_t));

    assert(node != NULL);

    node->key = key;
    node->content = content;
    node->right = node->left = node->parent = NULL;

    return node;
}


/**
 * Description: Insert into the binary tree using `node` as root.
 *
 * @param node The node to be used as top one.
 * @param key The key associated to the content.
 * @param content The content to be inserted.
 *
 */
void insert_binary_tree_from_node(binary_tree_node_t *node, char *key, void *content){
    assert(node != NULL);

    int cmp = strcmp(node->key, key);

    /** @todo Define behaviour on key collision 
     *  @note Current behaviour on key collision: value overwrite */
    if (cmp == 0){
	node->content = content;
    }
    else if (cmp < 0){
	if (node->left != NULL){
	    insert_binary_tree_from_node(node->left, key, content);
	}
	else{
	    binary_tree_node_t *new_node = make_binary_tree_node(key, content);
	    new_node->parent = node;
	    node->left = new_node;
	}
    }
    else{
	if (node->right != NULL){
	    insert_binary_tree_from_node(node->right, key, content);
	}
	else{
	    binary_tree_node_t *new_node = make_binary_tree_node(key, content);
	    new_node->parent = node;
	    node->right = new_node;
	}
    }
}


/**
 * Description: Insert into binary tree.
 * 
 * @param t Binary tree.
 * @param key The key for which the content is to be search.
 * @param content The content to insert.
 *
 */
void insert_binary_tree(binary_tree_t *t, char *key, void *content){
    assert(t != NULL);
    assert(key != NULL);

    if (t->top == NULL){
	t->top = make_binary_tree_node(key, content);
    }
    else{
	insert_binary_tree_from_node(t->top, key, content);
    }
}


/**
 * Description: Retrieves an element from a binary tree using `node` as root.
 *
 * @param node The node to be used as the top one.
 * @param key The key associated to the content.
 *
 * @return The content associated to the key or NULL if not found.
 *
 */
binary_tree_node_t *get_binary_tree_from_node(binary_tree_node_t *node, char *key){
    assert(node != NULL);

    int cmp = strcmp(node->key, key);
    if (cmp == 0){
	return node;
    }
    else{
	binary_tree_node_t *next_node;
	if (cmp < 0){
	    next_node = node->left;
	}
	else{
	    next_node = node->right;
	}

	if (next_node == NULL){
	    return NULL;
	}
	else{
	    return get_binary_tree_from_node(next_node, key);
	}
    }
}


/**
 * Description: Retrieves an element from a binary tree.
 *
 * @param t The binary tree to retrieve the data from.
 * @param key The key associated to the content.
 *
 * @return The content associated to the key or NULL if not found.
 *
 */
void *get_binary_tree(binary_tree_t *t, char *key){
    assert(t != NULL);
    assert(key != NULL);

    if (t->top == NULL){
	return NULL;
    }
    
    return get_binary_tree_from_node(t->top, key)->content;    
}


/**
 * Description: Replaces a node pointer with another one.
 *
 * @param parent The parent node.
 * @param node The child node to be replaced.
 * @param replacement The pointer which will replace `node`.
 *
 */
void replace_node_with(binary_tree_t *t,
		       binary_tree_node_t *parent,
		       binary_tree_node_t *node,
		       void *replacement){
    if (parent != NULL){
	if (parent->right == node){
	    parent->right = replacement;
	}
	else if (parent->left == node){
	    parent->left = replacement;
	}
    }
    else if(t->top == node){
	t->top = replacement;
    }
}


/**
 * Description: Finds the leftmost node from `node`.
 *
 * @param node The node where to start searching.
 *
 * @return The leftmost binary tree node.
 *
 */
binary_tree_node_t *get_all_left_binary_tree_node(binary_tree_node_t *node){
    assert(node != NULL);

    if (node->left == NULL){
	return node;
    }
    else{
	return get_all_left_binary_tree_node(node->left);
    }
}


/**
 * Description: Removes a node from the binary tree.
 *
 * @param t The binary tree to remove the data from.
 * @param node The node to be removed.
 * @param free_content_f A function to free the value (NULL for none).
 * @param free_key_f A function to free the key (NULL for none).
 *
 */
void rm_binary_tree_node(binary_tree_t *t,
			 binary_tree_node_t *node,
			 void (* free_content_f)(void *),
			 void (* free_key_f)(char *)){
    assert(t != NULL);
    assert(node != NULL);
    int free_node = 1;

    if ((node->right == NULL) && (node->left == NULL)){
	if (t->top == node){
	    t->top = NULL;
	}
	else{
	    assert(node->parent != NULL);
	    replace_node_with(t, node->parent, node, NULL);
	}
    }
    else if (node->right == NULL){
	replace_node_with(t, node->parent, node, node->left);
	node->left->parent = node->parent;
	if (t->top == node){
	    t->top = node->left;
	}
    }
    else if (node->left == NULL){
	replace_node_with(t, node->parent, node, node->right);
	node->right->parent = node->parent;
	if (t->top == node){
	    t->top = node->right;
	}
    }
    else{
	free_node = 0;
	// Left and right here are arbitrary.
	binary_tree_node_t *successor = 
	    get_all_left_binary_tree_node(node->right);

	if (free_content_f != NULL){
	    free_content_f(node->content);
	}
	if (free_key_f != NULL){
	    free_key_f(node->key);
	}

	node->content = successor->content;
	node->key = successor->key;

	// Avoid freeing it
	successor->content = NULL;
	successor->key = NULL;

	rm_binary_tree_node(t, successor, free_content_f, free_key_f);
    }

    if (free_node){
	if (free_content_f != NULL){
	    free_content_f(node->content);
	}
	if (free_key_f != NULL){
	    free_key_f(node->key);
	}
	free(node);
    }
}


/**
 * Description: Removes a node from the binary tree based on its key.
 *
 * @param t The binary tree to remove the data from.
 * @param key The key of the element to be removed.
 * @param free_content_f A function to free the value (NULL for none).
 * @param free_key_f A function to free the key (NULL for none).
 *
 */
void rm_binary_tree(binary_tree_t *t,
		    char *key,
		    void (* free_content_f)(void *),
		    void (* free_key_f)(char *)){

    assert(t != NULL);

    binary_tree_node_t *node = get_binary_tree_from_node(t->top, key);
    //fprintf(stderr, ">> %s\n", node->key);

    if (node != NULL){
	rm_binary_tree_node(t, node, free_content_f, free_key_f);
    }
}


/**
 * Description: Frees a binary tree node and all its childs.
 *
 * @param node The node to be freed.
 * @param free_content_f A function to free the values (NULL for none).
 * @param free_key_f A function to free the keys (NULL for none).
 *
 */
void free_binary_tree_from_node(binary_tree_node_t *node,
				void (* free_content_f)(void *),
				void (* free_key_f)(char *)){
    assert(node != NULL);

    if (free_content_f != NULL){
	free_content_f(node->content);
    }
    if (free_key_f != NULL){
	free_key_f(node->key);
    }

    if (node->left != NULL){
	free_binary_tree_from_node(node->left,
				   free_content_f,
				   free_key_f);
    }
    if (node->right != NULL){
	free_binary_tree_from_node(node->right,
				   free_content_f,
				   free_key_f);
    }
    
    free(node);
}


/**
 * Description: Frees a binary tree.
 *
 * @param t The tree to be freed.
 * @param free_content_f A function to free the values (NULL for none).
 * @param free_key_f A function to free the keys (NULL for none).
 *
 */
void free_binary_tree(binary_tree_t *t,
		      void (* free_content_f)(void *),
		      void (* free_key_f)(char *)){
    assert(t != NULL);

    if (t->top != NULL){
	free_binary_tree_from_node(t->top,
				   free_content_f,
				   free_key_f);
    }
    free(t);
}



#endif
