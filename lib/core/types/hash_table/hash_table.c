#ifndef HASH_TABLE_C
#define HASH_TABLE_C

#include "hash_table.h"
#include "binary_tree.h"
#include <stdio.h>

/**
 * @file hash_table.c
 *
 * @brief Almost Ansi C hash table implementation.
 * 
 * An ANSI C implementation of a hash table  allowing to use integers and
 * '\0' ended strings as index.
 * 
 * @note Uses One-at-a-Time hash algorithm for strings.
 * 
 * @todo Assert mallocs
 */

/**
 * Implementation specific data types
 * (internal structure for hash collisions).
 */
 
/**
 * Hash table node structure.
 * 
 */
struct hash_table_node{
    binary_tree_t *string_indexed;
};


/**
 * Hash table structure.
 * 
 */ 
struct hash_table{
    int size;    
    hash_table_node_t **fields;
    int hash_seed;
    llist_t *str_keys;
};


typedef struct hash_table_compound{
    llist_node_t *key_node;
    void *content;
}hash_table_compound_t;

/**
 *  Hashing code
 *  Returns the hash for a given variable using
 *  "One at a time hash".
 */
/**
 * Description: Returns the hash associated to an string.
 * 
 * @param s The string to be hashed.
 * 
 * @return The associated hash to s.
 * 
 */
hash_t get_hash(char *s, int seed){
    hash_t h = seed;
    int i;

    for (i = 0; s[i] != '\0'; i++ ){
        h += s[i];
        h += ( h << 10 );
        h ^= ( h >> 6 );
    }
 
    h += ( h << 3 );
    h ^= ( h >> 11 );
    h += ( h << 15 );
    
    return h;
}


/* Hash table creation/freeing */
/**
 * Description: Returns a hash table.
 * 
 * @return A hash table.
 * 
 */
hash_table_t create_hash_table(){
    return create_hash_table_with_size(DEFAULT_HASH_TABLE_SIZE);
}


/**
 * Description: Returns a hash table with the specified size.
 * 
 * @note Hash table sizes should be prime numbers for better performance.
 * 
 * @return A hash table.
 * 
 */
hash_table_t create_hash_table_with_size(int size){
    hash_table_t table = (hash_table_t) malloc(sizeof(struct hash_table));
    assert(table != NULL);
    
    table->size = size;
    table->fields = (hash_table_node_t **)
                          malloc(size * sizeof(hash_table_node_t *));
    table->hash_seed = rand();

    assert(table->fields != NULL);
    
    if (NULL == 0){
        bzero(table->fields, sizeof(hash_table_node_t *) * size);
    }
    else{
        int i;
        for (i = 0;i < size;i++){
            table->fields[(unsigned int) i] = NULL;
        }
    }

    table->str_keys = create_llist();
    assert(table->str_keys != NULL);

    return table;    
}


/**
 * Description: Frees a node from the hash table.
 * 
 * @param node The binary tree node to be freed.
 * @param free_content_f A function to free the values (NULL for none).
 * @param free_str_index_f A function to free
 *                         the string indexes (NULL for none).
 * 
 */
void free_hash_table_node(hash_table_node_t * node,
                          void (* free_content_f) (void *),
                          void (* free_str_index_f) (char *)){

    // With this we can simulate a closure over free_content_f
    // without resorting to global variables
    void free_ht_node(void *node){
	if (free_content_f != NULL){
	    free_content_f(((hash_table_compound_t *)node)->content);
	}
	free(node);
    }

    assert(node != NULL);    

    free_binary_tree(node->string_indexed,
		     free_ht_node,
		     free_str_index_f);

    free(node);
}


/**
 * Description: Frees a hash table.
 * 
 * @param table The hash table to be freed.
 * @param free_content_f A function to free the values (NULL for none).
 * @param free_str_index_f A function to free.
 *                         the string indexes (NULL for none).
 * 
 */
void free_hash_table(hash_table_t table,
                     void (* free_content_f) (void *),
                     void (* free_string_index_f) (char *)) {

    if(table == NULL){
        return ;
    }

    hash_table_node_t *node;
    int i;

    for (i = 0;i < table->size; i++){
        node = table->fields[(unsigned int) i];
        if (node != NULL){
            free_hash_table_node(node, free_content_f, free_string_index_f);
        }
    }
    free(table->fields);
    free_llist(table->str_keys, NULL);

    free(table);
}


/* Hash table manipulation */
/**
 * Description: Creates a hash table node.
 * 
 * @param hash The hash asociated to the node.
 * 
 * @return A pointer to a hash_table_node_t with the specified hash and
 *          NULLed 'left', 'right', 'integer_indexed', 'string_indexed'.
 * 
 */
hash_table_node_t * create_hash_table_node(hash_t hash){

    hash_table_node_t * node = (hash_table_node_t*)
                                malloc(sizeof(hash_table_node_t));

    node->string_indexed = create_binary_tree();

    return node;
}


/**
 * Description: Inserts a hash table node in a hash table or returns
 *  the one with the associated hash.
 * 
 * @param table The hash table to lookup in.
 * @param hash  The associated hash to the node.
 * 
 * @return A pointer to a hash_table_node_t.
 * 
 */
hash_table_node_t * _insert_hash_table_node(hash_table_t table, hash_t hash){

    hash_table_node_t * node = table->fields[(unsigned int) hash % table->size];

    if (node == NULL){
        node = table->fields[(unsigned int) hash % table->size] = create_hash_table_node(hash);
    }
    
    return node;
}


/**
 * Description: Creates a hash table compound.
 *
 * @param key_node The key node.
 * @param content The inserted content.
 *
 * @return hash_table_compound_t.
 *
 */
hash_table_compound_t *create_hash_table_compound(llist_node_t *key_node, void *content){
    hash_table_compound_t *htc = (hash_table_compound_t*) malloc(sizeof(hash_table_compound_t));
    assert(htc != NULL);

    htc->key_node = key_node;
    htc->content = content;

    return htc;
}


/**
 * Description: Inserts a value indexed by a string into the hash table.
 * 
 * @param table The hash table to lookup in.
 * @param s     The table index.
 * @param v     The value to insert.
 * 
 */
void insert_hash_table(hash_table_t table, char *s, void *v){

    hash_t hash = get_hash(s, table->hash_seed);

    hash_table_node_t * rnode = _insert_hash_table_node(table, hash);

    hash_table_compound_t *compound = create_hash_table_compound(add_to_llist(table->str_keys, s),
							  v);
    insert_binary_tree(rnode->string_indexed, s, compound);
}


/**
 * Description: Obtains the hash table node associated with a hash.
 * 
 * @param table The hash table to lookup in.
 * @param hash  The target hash.
 *
 * @return A pointer to the hash_table_node_t associated with the hash.
 * 
 */
hash_table_node_t *_get_values_for_hash(hash_table_t table, hash_t hash){
    return table->fields[(unsigned int) hash % table->size];
}


/**
 * Description: Obtains the value associated with a string in the hash table.
 * 
 * @param table The hash table to lookup in.
 * @param key   The key string.
 *
 * @return The inserted value or NULL if not found.
 * 
 */
void *get_hash_table(hash_table_t table, char *key){
    if (key == NULL){
        return NULL;
    }

    hash_t hash = get_hash(key, table->hash_seed);
    hash_table_node_t * rnode = _get_values_for_hash(table, hash);
    if (rnode == NULL){
        return NULL;
    }
    if (rnode->string_indexed == NULL){
        return NULL;
    }

    hash_table_compound_t *htc = (hash_table_compound_t*) get_binary_tree(rnode->string_indexed, key); 

    if (htc == NULL){
	return NULL;
    }
    
    return htc->content;
}


/**
 * Description: Obtains string key values of a hash table.
 * 
 * @param table The hash table to lookup in.
 *
 * @return The linked list of the string keys.
 *
 */
llist_t *get_hash_table_str_keys(hash_table_t table){
    return table->str_keys;
}


/**
 * Description: Remove a value indexed by a key.
 * 
 * @param table The hash table to lookup in.
 * @param key   The key of the value to remove.
 * @param free_content_f A function to free the values (NULL for none).
 * @param free_str_index_f A function to free.
 *                         the string indexes (NULL for none).
 * 
 * @return Has the key been found.
 */
int rm_hash_table(hash_table_t table,
		  char *key,
		  void (* free_content_f) (void *),
		  void (* free_str_index_f) (char *)){

    // With this we can simulate a closure over free_content_f
    // without resorting to global variables
    void free_ht_node(void *node){
	if (free_content_f != NULL){
	    free_content_f(((hash_table_compound_t *)node)->content);
	}
	free(node);
    }


    hash_t hash = get_hash(key, table->hash_seed);

    hash_table_node_t *bucket = _get_values_for_hash(table, hash);

    if (bucket == NULL){
	return 0;
    }

    rm_binary_tree(bucket->string_indexed,
		   key,
		   free_ht_node,
		   free_str_index_f);

    return 1;
}


#endif
