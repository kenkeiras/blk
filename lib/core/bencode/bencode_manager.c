#ifndef BENCODE_MANAGER_C
#define BENCODE_MANAGER_C

#include "bencode.h"
#include <assert.h>

/**
 * @file bencode_manager.c
 *
 * @brief Bencoded data manager.
 * 
 */


/**
 * Description: Frees a bencoded data tree.
 * 
 * @param b bencoded data.
 * 
 */
void free_bencoded(bencoded_t * b){
    if (b == NULL){
        return;
    }
    
    if (b->type == LIST_TYPE){
        free_llist((llist_t *)b->v.p, (void (*) (void *)) free_bencoded);
    }
    else if (b->type == STR_TYPE){
        free(((struct binary_string*)b->v.p)->s);
        free(b->v.p);
    }
    else if (b->type == DICT_TYPE){
        free_hash_table((hash_table_t)b->v.p,
                        (void (*) (void *)) free_bencoded,
                        (void (*) (char *)) free);
    }
    else{
        assert(b->type == INT_TYPE);
    }
    
    free(b);
}


#endif
