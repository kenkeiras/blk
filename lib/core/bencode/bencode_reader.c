#ifndef BENCODE_READER_C
#define BENCODE_READER_C

#include "bencode.h"
#include <assert.h>

/**
 * @file bencode_reader.c
 *
 * @brief Bencoded file reader.
 * 
 */

/**
 * Size of a single reader step.
 * 
 */
#ifndef READER_BUFFER_SIZE
#define READER_BUFFER_SIZE 40960
#endif

/**
 * Buffered reader structure.
 * 
 */
struct buffered_reader{
    FILE *f;
    int buffsize;
    int size;
    int pos;
    char *buffer;
    int eof;
};


/**
 * Description: Returns the smallest integer.
 * 
 * @param a A integer.
 * @param b Another integer.
 * 
 * @return the smallest between 'a' and 'b'.
 *
 */
int imin(int a, int b){
    return (a < b? a : b);
}


/**
 * Description: Checks if a character is a number representation.
 * 
 * @param c The character to evaluate.
 * 
 * @return 1 if is between '0' and '9' character, else 0.
 *
 */
int is_number(int c){
    return ((c >= '0') && (c <= '9'));
}


/**
 * Description: Converts a character encoded integer to integer.
 * 
 * @param c  the character to be converted.
 * @assert c between '0' and '9'.
 * 
 * @return The associated integer.
 *
 */
int to_integer(int c){
    return c - '0';
}


/**
 * Description: Reads one byte from the reader.
 * 
 * @param reader The buffered reader to read from.
 * 
 * @return The read byte.
 *
 */
int bfgetc(struct buffered_reader * reader){
    if (reader->eof){
        return EOF;
    }
  
    int c;
    if (reader->pos >= reader->size){
        reader->pos = 0;
        reader->size = fread(reader->buffer, sizeof(char),
                             reader->buffsize, reader->f);

        if (reader->size == 0){
            reader->eof = 1;
            return EOF;
        }
    }
    
    c = reader->buffer[reader->pos];
    reader->pos++;
    
    return c;
}


/**
 * Description: Reads several bytes from the reader.
 * 
 * @param reader The buffered reader to read from.
 * @param buffer The buffer to write on.
 * @param size The size to be read.
 * 
 * @return The read byte number.
 *
 */
int bfread(struct buffered_reader * reader, char *buffer, int size){
    
    int buffered = imin(size, reader->size - reader->pos);
    strncpy(buffer, &(reader->buffer[reader->pos]), buffered);

    int remaining = size - (buffered);
    reader->pos += size;
    
    if (remaining > 0){
        return buffered + fread(&(buffer[buffered]),
                                    sizeof(char), remaining, reader->f);
    }
    else{
        return size;
    }

}


/* Forward definition */
/**
 * Description: Reads a bencoded element from the reader.
 * 
 * @param reader The buffered reader to read from.
 * @param end A pointer to an integer, set to 1 if is found an end
 *            element or 0 if not.
 * 
 * @return The bencoded element.
 *
 */
bencoded_t * read_bencoded_element(struct buffered_reader * reader, int* end);


/**
 * Description: Creates a bencoded dictionary.
 * 
 * @return Bencoded dictionary.
 * 
 */
bencoded_t * create_bencoded_dictionary(){
    bencoded_t * b = (bencoded_t *) malloc(sizeof(bencoded_t));
    assert(b != NULL);
    b->type = DICT_TYPE;
    b->v.p = create_hash_table();
    
    return b;
}


/**
 * Description: Creates a bencoded list.
 * 
 * @return Bencoded dictionary.
 * 
 */
bencoded_t * create_bencoded_list(){
    bencoded_t * b = (bencoded_t *) malloc(sizeof(bencoded_t));
    assert(b != NULL);
    b->type = LIST_TYPE;
    b->v.p = create_llist();
    
    return b;
}


/**
 * Description: Creates a bencoded integer.
 * 
 * @return Bencoded integer.
 * 
 */
bencoded_t * create_bencoded_integer(){
    bencoded_t * b = (bencoded_t *) malloc(sizeof(bencoded_t));
    assert(b != NULL);
    b->type = INT_TYPE;
    
    return b;
}


/**
 * Description: Creates a bencoded integer with a specific value.
 * 
 * @return Bencoded integer.
 * 
 */
bencoded_t * create_bencoded_integer_from(int value){
    bencoded_t * b = create_bencoded_integer();
    b->v.n = value;
        
    return b;
}


/**
 * Description: Obtains a pointer to the string of a bencoded string.
 * 
 * @param b Bencoded string to be "unwrapped".
 * 
 * @return A pointer to the actual string.
 * 
 */
char *get_bencoded_string(bencoded_t *b){
    assert(b->type == STR_TYPE);
    struct binary_string *bs = (struct binary_string *)b->v.p;
    
    return bs->s;
}


/**
 * Description: Obtains a pointer to the linked list from a bencoded list.
 * 
 * @param b Bencoded list to be "unwrapped".
 * 
 * @return A pointer to the actual list.
 * 
 */
llist_t *get_bencoded_llist(bencoded_t *b){
    assert(b->type == LIST_TYPE);
    
    return (llist_t *) b->v.p;
    
}


/**
 * Description: Obtains a integer from a bencoded structure.
 * 
 * @param b Bencoded integer to be "unwrapped".
 * 
 * @return The integer contained in b.
 * 
 */
int get_bencoded_integer(bencoded_t *b){
    assert(b->type == INT_TYPE);
    
    return (int) b->v.n;    
} 


/**
 * Description: Obtains a hash table from a bencoded structure.
 * 
 * @param b Bencoded hash table to be "unwrapped".
 * 
 * @return The hash table contained in b.
 * 
 */
hash_table_t get_bencoded_dictionary(bencoded_t *b){
    assert(b->type == DICT_TYPE);
    
    return (hash_table_t) b->v.p;
} 


/**
 * Description: Reads a bencoded integer from the reader.
 * 
 * @param reader The buffered reader to read from.
 * 
 * @return The bencoded integer.
 *
 */
bencoded_t * read_bencoded_integer(struct buffered_reader * reader){
    bencoded_t * b = (bencoded_t *) malloc(sizeof(bencoded_t));
    assert(b != NULL);
    b->type = INT_TYPE;

    int c;
    int num = 0;
    c = bfgetc(reader);
    while (c != 'e' && c != EOF){
        num *= 10;
        num += to_integer(c);
        c = bfgetc(reader);
    }
    
    b->v.n = num;
    return b;
}


/**
 * Description: Reads a bencoded string from the reader.
 * 
 * @param reader The buffered reader to read from.
 * @param size The first char of the length string.
 * 
 * @return The bencoded string.
 *
 */
bencoded_t * read_bencoded_string_starting_with(struct buffered_reader * reader,
                                                int size){

    bencoded_t * b = (bencoded_t *) malloc(sizeof(bencoded_t));
    assert(b != NULL);
    b->type = STR_TYPE;
    
    size = to_integer(size);

    int c;
    c = bfgetc(reader);
    while (c != ':' && c != EOF){
        size *= 10;
        size += to_integer(c);
        c = bfgetc(reader);
    }

    b->v.p = malloc(sizeof(struct binary_string));

    ((struct binary_string*) b->v.p)->size = size;

    /* Leave one extra char for a trailing '\0' */
    ((struct binary_string*) b->v.p)->s = (char *) malloc(
                                                     sizeof(char) * (size + 1));
                                                     

    
    int has_read = bfread(reader, ((struct binary_string*)b->v.p)->s, size);

    ((struct binary_string*)b->v.p)->s[has_read] = '\0';
    assert(has_read == size);
    
    return b;
}


/**
 * Description: Reads a bencoded string from the reader.
 * 
 * @param reader The buffered reader to read from.
 * 
 * @return The bencoded element.
 *
 */
bencoded_t * read_bencoded_string(struct buffered_reader * reader){
    return read_bencoded_string_starting_with(reader, bfgetc(reader));
}


/**
 * Description: Reads a bencoded list from the reader.
 * 
 * @param reader The buffered reader to read from.
 * 
 * @return The bencoded list.
 *
 */
bencoded_t * read_bencoded_list(struct buffered_reader * reader){
    bencoded_t * b = create_bencoded_list();
    
    bencoded_t *e;
    int end;
    e = read_bencoded_element(reader, &end);
    while (!end){
        assert(e != NULL);

        add_to_llist((llist_t*) b->v.p, e);
        e = read_bencoded_element(reader, &end);
    }

    return b;
}


/**
 * Description: Reads a bencoded dictionary.
 * 
 * @param reader The buffered reader to read from.
 * 
 * @return The bencoded dictionary.
 *
 */
bencoded_t * read_bencoded_dictionary(struct buffered_reader * reader){
    bencoded_t * b = create_bencoded_dictionary();

    bencoded_t *index, *element;
    int end;
    
    index = read_bencoded_element(reader, &end);
    while (!end){
        assert(index != NULL);
        element = read_bencoded_element(reader, &end);
        
        assert(!end);
        assert(element != NULL);
          
        
        /* Asume that it's a string, get the buffer, free the other memory and 
         * insert it into the hash table */
        assert(index->type == STR_TYPE);
        char *s = get_bencoded_string(index);
        free(index->v.p);
        free(index);

        insert_hash_table((hash_table_t)(b->v.p), s, (void *) element);
        
        index = read_bencoded_element(reader, &end);
    }
    return b;
}


/**
 * Description: Reads a bencoded element from the reader.
 * 
 * @param reader The buffered reader to read from.
 * @param end A pointer to an integer, set to 1 if is found an end
 *            element or 0 if not.
 * 
 * @return The bencoded element.
 *
 */
bencoded_t * read_bencoded_element(struct buffered_reader * reader, int* end){
    int c = bfgetc(reader);
    
    *end = 0;

    switch(c){
        case 'i':
            return read_bencoded_integer(reader);
        
        case 'd':
            return read_bencoded_dictionary(reader);
            
        case 'l':
            return read_bencoded_list(reader);
            
        case 'e':
            *end = 1;
            return NULL;
        
        default:
            if (is_number(c)){
                return read_bencoded_string_starting_with(reader, c);
            }
    }
    return NULL;
}


/**
 * Description: Reads a bencoded file.
 * 
 * @param f Pointer to the file to be read (assorted to be open with 'b').
 * @param size Reader buffer size.
 * 
 * @return Bencoded element.
 * 
 */
bencoded_t * read_bencoded_with_buffer_size(FILE *f, int size){
    
    if (f == NULL){
        return NULL;
    }
    
    /* Buffered file reader initialization */
    struct buffered_reader * reader = (struct buffered_reader *)
                                         malloc(sizeof(struct buffered_reader));

    int end;

    assert(reader != NULL);
    reader->buffer = (char *) malloc(size * sizeof(char)); /* Reading buffer */

    assert(reader->buffer != NULL);

    reader->f = f;           /* File pointer */
    reader->buffsize = size; /* Buffer size */
    reader->pos = 0;         /* Position in the buffer */
    reader->size = 0;        /* Size of the data in the buffer */
    reader->eof = 0;         /* Reached end of file ? */

    /* Bencoded file reading */
    bencoded_t * b = read_bencoded_element(reader, &end);

    assert(!end);

    /* Data freeing */
    free(reader->buffer);
    free(reader);
    
    return b;
}


/**
 * Description: Reads a bencoded file.
 * 
 * @param f Pointer to the file to be read (assorted to be open with 'b').
 * 
 * @return Bencoded element.
 * 
 */
bencoded_t * read_bencoded(FILE *f){
    return read_bencoded_with_buffer_size(f, READER_BUFFER_SIZE);
}


#endif
