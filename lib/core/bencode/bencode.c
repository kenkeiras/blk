#ifndef BENCODE_C
#define BENCODE_C

/**
 * @file bencode.c
 *
 * @brief Bencode function wrapper.
 * 
 * A wrapper to the bencode reader, manager and writer.
 * 
 * @todo write a bencode writer.
 * 
 */


#include "bencode_manager.c"
#include "bencode_reader.c"
//#include "bencode_writer.c"

#endif
