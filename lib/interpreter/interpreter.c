#ifndef INTERPRETER_C
#define INTERPRETER_C

#include "interpreter.h"

/**
 * @file interpreter.c
 * 
 * @brief Basic BLK interpreter.
 * 
 * @todo Fully remove bencode structure dependencies.
 * @todo Implement heap usage.
 * 
 */

/* ******************* */
/* Forward declaration */
/**
 * Description: Executes a function frame with a defined operation list.
 *
 * @param bm The bytecode manager.
 * @param frame The frame to be executed.
 * @param code The operation list.
 * @param lfline The connection to the parent frames.
 * @param parent_stack A pointer to the parent frame stack.
 * @param stack_size The parent frame stack size.
 * @param heap The heap array.
 * @param do_break If the code is done with the current loop.
 * @param do_ret If the code is done with the current function.
 * @param params Function parameters.
 * 
 * @return The returned value (if any).
 * @note by default it returns 0.
 * 
 */
int execute(bytecode_manager_t *bm,
            bencoded_t *frame,
            llist_t *code,
            lifeline_t lfline,
            int **parent_stack,
            int stack_size,
            int *heap,
            int (*enviroment_extend) (int, int*, char*),
            int *do_break,
            int *do_ret,
            hash_table_t params);


/**
 * Description: Runs an operation in a code frame.
 * 
 * @param bm The bytecode manager.
 * @param op The operation to run.
 * @param locations The current frame value table.
 * @param frame  The current code frame.
 * @param lfline The connection to the parent frames.
 * @param stack Local variable memory.
 * @param heap Static content memory.
 * @param stack_size The size of the local memory.
 * @param enviroment_extend A function which extends those provided builtin by
 *                          the interpreter. 
 * @param return_value The value returned by the operation.
 * 
 * @return 1 if is a RETURN operation, 0 elsewhere.
 * 
 */
int run_operation(bytecode_manager_t *bm,
                    bencoded_t *op,
                    hash_table_t locations,
                    bencoded_t *frame,
                    llist_t *code,
                    lifeline_t lfline,
                    int **stack,
                    int *heap,
                    int stack_size,
                    int (*enviroment_extend) (int, int*, char*),
                    int *return_value);
                    
/* ************** */
/* Implementation */
/**
 * Description: Runs a built-in operation against some parameters, returning
 * the result.
 * 
 * @param op Operation code.
 * @param params Parameter value array.
 * @param param_num Number of parameters.
 * 
 */
int get_op_result(int op, int params[], int param_num){
    switch(op){
        /* Better implemented outside the function
        case RETURN_OP:
            assert(param_num == 1);
            abort();
        */
            
        case ASSIGNATION_OP:
            assert(param_num == 1);
            return params[0];

        /* Better implemented outside the function
        case GET_ADRESS_OP:
            assert(param_num == 1);
            abort();
        */
        
        case DEREFERENCE_OP:
            assert(param_num == 1);
            fprintf(stderr, "DEREFERENCE_OP not implemented\n");
            abort();

        case ADD_OP:
            assert(param_num == 2);
            return params[0] + params[1];
        
        case SUB_OP:
            assert(param_num == 2);
            return params[0] - params[1];

        case MUL_OP:
            assert(param_num == 2);
            return params[0] * params[1];

        case DIV_OP:
            assert(param_num == 2);
            return params[0] / params[1];

        case EQ_OP:
            assert(param_num == 2);
            return params[0] == params[1];

        case NE_OP:
            assert(param_num == 2);
            return params[0] != params[1];

        case LT_OP:
            assert(param_num == 2);
            return params[0] < params[1];

        case GT_OP:
            assert(param_num == 2);
            return params[0] > params[1];

        case LE_OP:
            assert(param_num == 2);
            return params[0] <= params[1];

        case GE_OP:
            assert(param_num == 2);
            return params[0] >= params[1];

        case NEGATION_OP:
            assert(param_num == 1);
            if (params[0] == 0){
                return 1;
            }
            else{
                return 0;
            }

        default:
            fprintf(stderr, "Unknown operation code %i\n", op);
            abort();            
    }
}


/**
 * Description: Obtains the position of a variable in the memory.
 * 
 * @param var_name The name of the variable.
 * @param positions Secuence of bencoded array positions to go throught.
 * @param locations The current frame value table.
 * @param frame  The current code frame.
 * @param lfline The connection to the parent frames.
 * @param stack Local variable memory.
 * @param heap Static content memory.
 * @param stack_size The size of the local memory.
 * 
 * @return The position of the variable in the memory.
 * 
 */
int lookup_variable_pos(char *var_name,
                          llist_t *positions,
                          hash_table_t locations,
                          bencoded_t  *frame,
                          lifeline_t lfline,
                          int *stack,
                          int *heap,
                          int stack_size){

    int found = 0;
    int pos = 0;
    do{
        if (frame_has_variable(var_name, frame)){
            int *value = (int *) get_hash_table(locations, var_name);
            
            found = 1;
            pos = (*value);
        }
        
        if (!found){
            if (lfline != NULL){

                locations = lfline->locations;
                frame = lfline->frame;
                lfline = lfline->parent;
            }
            else{
                locations = NULL;
            }
        }
    }while((!found) && (locations != NULL)); 
    
    if (!found){
        fprintf(stderr, "Variable '%s' not found\n", var_name);
        abort();
    }

    llist_node_t *node = NULL;
    int step;
    if (positions != NULL){
        node = positions->first;
    }
    while(node != NULL){
        bencoded_t *e = (bencoded_t *) node->element;
        
        step = get_value(e, locations, frame, lfline, stack, heap, stack_size);
        pos = stack[pos] + step;
        
        node = node->next;        
    }
    if ((pos > stack_size) || (pos < 0)){
        fprintf(stderr, "Access to position %i presumably to '%s' array,"
                        " out of bounds\n", pos/sizeof(int), var_name);
        abort();
    }

    return pos;
}

/**
 * Description: Looks for the value of a variable.
 * 
 * @param var_name The name of the variable.
 * @param position Secuence of bencoded array positions to go throught.
 * @param locations The current frame value table.
 * @param frame  The current code frame.
 * @param lfline The connection to the parent frames.
 * @param stack Local variable memory.
 * @param heap Static content memory.
 * @param stack_size The size of the local memory.
 * 
 * @return The value of the variable named 'var_name' or 0 if not found.
 * 
 */
int lookup_variable(char *var_name,
                    llist_t *positions,
                    hash_table_t locations,
                    bencoded_t  *frame,
                    lifeline_t lfline,
                    int *stack,
                    int *heap,
                    int stack_size){
                        
    int p = lookup_variable_pos(var_name, positions, locations, frame,
                                lfline, stack, heap, stack_size);


    return stack[p];
}

/**
 * Description: Obtains the value of a bencoded variable.
 * 
 * @param var The variable to "unwrap".
 * @param locations The current frame value table.
 * @param frame  The current code frame.
 * @param lfline The connection to the parent frames.
 * @param stack Local variable memory.
 * @param heap Static content memory.
 * @param stack_size The size of the local memory.
 * 
 * @return The integer value of 'var'.
 * 
 */
int get_value(bencoded_t *var,
              hash_table_t locations,
              bencoded_t  *frame,
              lifeline_t lfline,
              int *stack,
              int *heap,
              int stack_size){
                  

    char *endptr;
    int int_val;
    
    if (var->type == INT_TYPE){
        return get_bencoded_integer(var);
    }

    else if (var->type == STR_TYPE){
        int_val = strtol(get_bencoded_string(var), &endptr, 0);

        /* Is just a integer */
        if (*endptr == '\0'){
            return int_val;
        }
    }

    /* Static arrays not considered yet
    else if (var->type == DICT_TYPE){
        bencoded_t *mem = copy_mem_from_param

        return 0;
    }
    */

    llist_t *pos = NULL;
    char *var_name = NULL;

    if (var->type == DICT_TYPE){
        pos = get_var_pos(var);

        var_name = get_var_name(var);
    }
    else if(var->type == STR_TYPE){
        var_name = get_bencoded_string(var);
    }
    
    assert(var_name != NULL);

    return lookup_variable(var_name, pos, locations, frame, lfline,
                            stack, heap, stack_size);
}


/**
 * Description: Converts the bencoded function parameters to it's integer
 *  values.
 * 
 * @param params The parameter list.
 * @param frame The current code frame.
 * @param param_num A pointer to integer to define the parameter number.
 * @param locations The current frame value table.
 * @param lfline A lifeline_t to the parent frames.
 * @param stack Local variable memory.
 * @param heap Static content memory.
 * @param stack_size The size of the local memory.
 * 
 * @return A array of integers with the locations of the parameters.
 * 
 */
int *get_values(bencoded_t *params,
                bencoded_t *frame,
                int *param_num,
                hash_table_t locations,
                lifeline_t lfline,
                int *stack,
                int *heap,
                int stack_size){

    assert(params != NULL);
    int *r = NULL;

    if(params->type == LIST_TYPE){
        llist_t *plist = get_bencoded_llist(params);

        *param_num = plist->size;
        r = (int *) malloc(sizeof(int) * (*param_num));
        int i;
        llist_node_t *node = plist->first;
        for(i = 0;node != NULL; i++){
            assert(i < (*param_num));

            bencoded_t *b = (bencoded_t *) node->element;
            
            r[i] = get_value(b, locations, frame, lfline,
                                        stack, heap, stack_size);
            
            node = node->next;
        }
        
        assert(i == (*param_num));
    }
    else{
        *param_num = 1;
        r = (int *) malloc(sizeof(int));
        r[0] = get_value(params, locations, frame, lfline,
                         stack, heap, stack_size);
    }
    
    return r;

}


/**
 * Description: Defines the value of a bencoded variable.
 * 
 * @param var The variable to "unwrap".
 * @param v   The value to be set.
 * @param locations The current frame value table.
 * @param frame  The current code frame.
 * @param lfline The connection to the parent frames.
 * @param stack Local variable memory.
 * @param heap Static content memory.
 * @param stack_size The size of the local memory.
 * 
 */
void set_value(bencoded_t *var,
              int v,
              hash_table_t locations,
              bencoded_t  *frame,
              lifeline_t lfline,
              int *stack,
              int *heap,
              int stack_size){

    /* Cannot assign one integer to another ( 7 = 12 ? ) */
    if (var->type == INT_TYPE){
        return;
    }

    llist_t *pos = NULL;
    char *var_name = NULL;

    if (var->type == DICT_TYPE){
        pos = get_var_pos(var);

        var_name = get_var_name(var);
    }
    else if(var->type == STR_TYPE){
        var_name = get_bencoded_string(var);
    }
    
    if (var_name != NULL){

        int p = lookup_variable_pos(var_name, pos,
                                    locations, frame,
                                    lfline, stack, heap, stack_size);

        stack[p] = v;
    }
}


/**
 * Description: Returns a "pointer" to the variable reference within the
 *   memory frame.
 * 
 * @param var The variable to "unwrap".
 * @param locations The current frame value table.
 * @param frame  The current code frame.
 * @param lfline The connection to the parent frames.
 * @param stack Local variable memory.
 * @param heap Static content memory.
 * @param stack_size The size of the local memory.
 * 
 * @return The memory position where the variable is.
 * 
 */
int get_position(bencoded_t *var,
                  hash_table_t locations,
                  bencoded_t  *frame,
                  lifeline_t lfline,
                  int *stack,
                  int *heap,
                  int stack_size){

    char *var_name = NULL;
    llist_t *pos = NULL;
    if (var->type == DICT_TYPE){
        pos = get_var_pos(var);

        var_name = get_var_name(var);
    }
    else{
        assert(var->type == STR_TYPE);
        var_name = get_bencoded_string(var);
    }
    
    assert(var_name != NULL);

    int p = lookup_variable_pos(var_name, pos,
                                locations, frame,
                                lfline, stack, heap, stack_size);

    return p;
}


/**
 * Description: Considers an array in the stack memory allocation. It also
 * links the different array dimensions.
 * 
 * @param stack A pointer to the stack (so more memory can be allocated).
 * @param stack_size A pointer to the stack size (so it can be adjusted).
 * @param pos A linked list node, used in n dimensional arrays (just call it
 *              with the first element of dimensions).
 * @param locations The current frame value table.
 * @param frame  The current code frame.
 * @param heap Static memory content.
 * @param lfline The connection to the parent frames.
 * 
 * @return The position where the first element of the uppermost dimension can
 *           be found.
 */
int add_to_stack(int **stack,
                 int *stack_size,
                 llist_node_t *pos,
                 hash_table_t locations,
                 bencoded_t *frame,
                 int *heap,
                 lifeline_t lfline){

    int dimension, i, base_pos = 0;

    if (pos != NULL){
        dimension = get_value((bencoded_t *) pos->element, locations, frame,
                              lfline, *stack, heap, *stack_size);

        if (dimension > 0){
            base_pos = *stack_size;

            *stack_size += dimension;
            *stack = (int *) realloc(*stack, (*stack_size) * sizeof(int));
            assert((*stack) != NULL);
            
            for (i = 0; i < dimension; i++){
                (*stack)[base_pos + i] = add_to_stack(stack, stack_size,
                                                      pos->next, locations,
                                                      frame, heap, lfline);
            }
        }
    }
    
    return base_pos;
}


/**
 * Description: Executes a operation list within a frame.
 * 
 * @param bm The bytecode manager.
 * @param locations A hashtable specifying the local variable positions.
 * @param frame  The current frame.
 * @param code   A linked list containing the operations to run.
 * @param lfline A lifeline_t to be able to retrieve parent frame variables.
 * @param stack  A pointer to the frame local memory.
 * @param heap   The static memory.
 * @param stack_size The size of the local memory.
 * @param enviroment_extend A function which extends those provided builtin by
 *                          the interpreter. 
 * 
 * @return The returned value from the code frame.
 * 
 */
int execute_code_list(bytecode_manager_t *bm,
                      hash_table_t locations,
                      bencoded_t *frame,
                      llist_t *code,
                      lifeline_t lfline,
                      int **stack,
                      int *heap,
                      int stack_size,
                      int (*enviroment_extend) (int, int*, char*)){
    
    int done = 0;
    int return_value = 0;
    bencoded_t *op;

    done = 0;

    /* Take first operation */
    llist_node_t *node = code->first;
   

    /* Iterate over operations */
    while ((node != NULL) && (!done)){
        op = (bencoded_t *) node->element;
        
       
        fflush(stdout);
        /* And execute it until the function or whatever is done */
        done = run_operation(bm, op, locations, frame, code, lfline, stack,
                              heap, stack_size, enviroment_extend,
                              &return_value);

       
        fflush(stdout);
        /* Takes next operations */
        node = node->next;
    }
   

    return return_value;
}


/**
 * Description: Evaluates a branching condition.
 * 
 * @param op The operation where the condition is.
 * @param locations The current frame value table.
 * @param frame  The current code frame.
 * @param lfline The connection to the parent frames.
 * @param stack Local variable memory.
 * @param heap Static content memory.
 * @param stack_size The size of the local memory.
 * 
 * @return 0 for False, 1 for True.
 * 
 */
int is_condition_ok(bencoded_t *op,
                    hash_table_t locations,
                    bencoded_t *frame,
                    lifeline_t lfline,
                    int *stack,
                    int *heap,
                    int stack_size){

        int val = get_value(get_op_condition(op), locations, frame, lfline,
                            stack, heap, stack_size);

        return val != 0;
}


/**
 * Description: Stores the function parameters into a hash table indexed by it's
 * name.
 * 
 * @param param_num Number of parameters.
 * @param param_arr Parameter containing the value array.
 * @param func      The function to where the values are passed.
 * 
 * @return A hash table containing the values indexed by the variable name. 
 * 
 */
hash_table_t allocate_params(int param_num, int *param_arr, bencoded_t *func){
    if ((func == NULL) || (param_arr == NULL) || (param_num < 0)){
        return NULL;
    }
    
    llist_t* params = get_function_params(func);
    assert(params != NULL);
    
    assert(params->size == param_num);
    
    hash_table_t ht = create_hash_table();
    assert(ht != NULL);

    llist_node_t *node = params->first;
    int i = 0;
    
    while (node != NULL){
        char *name = get_var_name((bencoded_t *) node->element);
        
        insert_hash_table(ht, name, create_bencoded_integer(param_arr[i]));
        
        node = node->next;
        i++;
    }

    return ht;
}


/**
 * Description: Frees the memory allocated by 'allocate_params'. 
 * 
 * @param params The hash table generated by 'allocate_params'.
 * 
 */
void free_params(hash_table_t params){
    free_hash_table(params, (void (*) (void *))free_bencoded, NULL);
}


/**
 * Description: Tries to call a function from within the code file.
 * 
 * @param bm The bytecode manager.
 * @param param_num Number of parameters given to de function.
 * @param param_arr Parameter containing the value array.
 * @param fname The function name.
 * @param locations The current frame value table.
 * @param frame The originating frame.
 * @param lfline The connection to the parent frames.
 * @param stack A pointer to the local memory.
 * @param heap  The global memory.
 * @param stack_size The stack size.
 * @param enviroment_extend A function which extends those provided builtin by
 *                          the interpreter. 
 * 
 * @return 1 if the function is found, 0 if it isn't.
 * 
 */
int call_function(bytecode_manager_t *bm,
                  int param_num,
                  int *param_arr,
                  char *fname, 
                  hash_table_t locations,
                  bencoded_t *frame,
                  lifeline_t lfline,
                  int **stack,
                  int *heap,
                  int stack_size,
                  int (*enviroment_extend) (int, int*, char*)){

    int found = 0;
    int do_break, do_ret;
    bencoded_t *function;
    
    do_break = do_ret = 0;
    do{
        if ((function = get_frame_function(fname, frame)) != NULL){

            llist_t *fun_code = get_code_from_frame(function);

            if (fun_code != NULL){
                hash_table_t params = allocate_params(param_num, param_arr,
                                                      function);
                
                found = 1;

                lifeline_t blfline = (lifeline_t) malloc(sizeof(struct lifeline));
                assert(blfline != NULL);

                blfline->parent = lfline;
                blfline->frame = frame;
                blfline->locations = locations;

                execute(bm, function, fun_code, blfline, stack, stack_size, heap,
                        enviroment_extend, &do_break, &do_ret, params);

                free(blfline);
                
                free_params(params);
            }
        }

        if (!found){
            if (lfline != NULL){

                frame = lfline->frame;
                lfline = lfline->parent;
            }
            else{
                frame = NULL;
            }
        }
    }while((!found) && (frame != NULL)); 
 
    return found;
}


/**
 * Description: Runs an operation in a code frame.
 * 
 * @param bm The bytecode manager.
 * @param op The operation to run.
 * @param locations The current frame value table.
 * @param frame  The current code frame.
 * @param lfline The connection to the parent frames.
 * @param stack Local variable memory.
 * @param heap Static content memory.
 * @param stack_size The size of the local memory.
 * @param enviroment_extend A function which extends those provided builtin by
 *                          the interpreter. 
 * @param return_value The value returned by the operation.
 * 
 * @return 1 if is a RETURN operation, 0 elsewhere.
 * 
 */
int run_operation(bytecode_manager_t *bm,
                    bencoded_t *op,
                    hash_table_t locations,
                    bencoded_t *frame,
                    llist_t *code,
                    lifeline_t lfline,
                    int **stack,
                    int *heap,
                    int stack_size,
                    int (*enviroment_extend) (int, int*, char*),
                    int *return_value){

    int ret_value = 0; // May be unset
    int op_code;
    int *param_arr;
    int param_num;
 

    bencoded_t *op_code_b;
    bencoded_t *params;
    bencoded_t *return_variable;

    /* Operation decoding */
    int group = get_op_group(op);

    /* *** Undefined operation *** */
    if (group == NO_GROUP){
        fprintf(stderr, "Undefined code group\n");
        abort();
    }

    /* *** Simple operation *** */
    else if (group == OP_GROUP){
        op_code_b = get_op_code(op);
        params = get_op_params(op);

        assert(op_code_b != NULL);

        /* Built-in operation */
        if (op_code_b->type == INT_TYPE){
            
            op_code = get_bencoded_integer(op_code_b);


            if (op_code == DEREFERENCE_OP){
                ret_value = get_position(params, locations, frame, 
                                          lfline, *stack, heap, stack_size);
            }
            
            else{

               
                param_arr = get_values(params, frame, &param_num, locations,
                                        lfline, *stack, heap, stack_size);
               
                if (op_code == RETURN_OP){

                    *return_value = param_arr[0];

                    free(param_arr);

                    return 1;
                }
                else{
                    ret_value = get_op_result(op_code, param_arr,
                                                        param_num);
                   
                                                        
                }
                
                free(param_arr);

            }
        }
        
        /* Non built-in operation */
        else{
            param_arr = get_values(params, frame, &param_num, locations,
                                    lfline, *stack, heap, stack_size);

            char *fname = get_op_function_name(op);

            int in_code = call_function(bm, param_num, param_arr, fname, 
                                        locations, frame, lfline, stack, heap,
                                        stack_size, enviroment_extend);

            /* Enviroment operation? */
            if ((!in_code) && (enviroment_extend != NULL)){
                enviroment_extend(param_num, param_arr, fname);
                                    
            }
            free(param_arr);
        }
        
        /* Saves the value of the operation to some variable */
        return_variable = get_op_return_var(op);
        if (is_meaningfull_variable(return_variable)){
            set_value(return_variable, ret_value, locations, frame, 
                        lfline, *stack, heap, stack_size);
        }
    }
    
    /* *** Branching operation *** */
    else if(group == BRANCH_GROUP){
        int btype = get_branch_type(op);
        
        int first = 1; /* First iteration ¿? */
        int do_ret, do_break; /* Do not try to do more iterations */
        llist_t *eval_code = get_branch_eval_code(op);
        llist_t *step_code = get_branch_step_code(op);
        llist_t *branch_code = get_branch_code(op);
        lifeline_t blfline = (lifeline_t) malloc(sizeof(struct lifeline));
        assert(blfline != NULL);

        blfline->parent = lfline;
        blfline->frame = frame;
        blfline->locations = locations;
       
        if ((!(btype == DO_WHILE_BRANCH)) && (first)){
            execute_code_list(bm, locations, frame, eval_code,
                              lfline, stack, heap, stack_size,
                              enviroment_extend);
            
        }

       
        do_break = do_ret = 0;
        /* Sorry for the mess :( */
        while ((((btype == DO_WHILE_BRANCH) && (first))
                 || (is_condition_ok(op, locations, frame, lfline,
                                      *stack, heap, stack_size)))
              && (!(do_break || do_ret))){

            first = 0;
            do_break = (btype == IF_BRANCH) || (btype == ELSE_BRANCH);

            execute(bm, op, branch_code, blfline, stack, stack_size, heap,
                    enviroment_extend, &do_break, &do_ret, NULL);

            if (!do_break){
                execute_code_list(bm, locations, frame, step_code,
                                  lfline, stack, heap, stack_size,
                                  enviroment_extend);

                execute_code_list(bm, locations, frame, eval_code,
                                  lfline, stack, heap, stack_size,
                                  enviroment_extend);
            }
        }
       
        free(blfline);
    }

    return 0;
}


/**
 * Description: Executes a function frame with a defined operation list.
 *
 * @param bm The bytecode manager.
 * @param frame The frame to be executed.
 * @param code The operation list.
 * @param lfline The connection to the parent frames.
 * @param parent_stack A pointer to the parent frame stack.
 * @param stack_size The parent frame stack size.
 * @param heap The heap array.
 * @param do_break If the code is done with the current loop.
 * @param do_ret If the code is done with the current function.
 * @param params Function parameters.
 * 
 * @return The returned value (if any).
 * @note by default it returns 0.
 * 
 */
int execute(bytecode_manager_t *bm,
            bencoded_t *frame,
            llist_t *code,
            lifeline_t lfline,
            int **parent_stack,
            int stack_size,
            int *heap,
            int (*enviroment_extend) (int, int*, char*),
            int *do_break,
            int *do_ret,
            hash_table_t params){

    hash_table_t locations;
    int *pos_p, base_pos, found;

    int return_value;
    
    int former_stack_size = stack_size;
    int *stack = NULL;
    
    /* Variable allocation */
    /* Stack allocation */
    bencoded_t *var_b;
    llist_t *var_list = get_frame_var_names(frame);

    stack_size += var_list->size;
    
    if (parent_stack == NULL){
        stack = (int *) malloc(stack_size * sizeof(int));
    }
    else{
        stack = (int *) realloc(*parent_stack, stack_size * sizeof(int));
    }
    
    if (stack_size > 0){
        assert(stack != NULL);
    }
    
    locations = create_hash_table();
    int i = former_stack_size;
    llist_node_t *var = var_list->first; 

    /* **************************************** */
    /* Position assignment and array allocation */
    while (var != NULL){

        pos_p = (int *) malloc(sizeof(int));
        *pos_p = i;
        
        found = FALSE;
        char *var_name = (char *) var->element;
        if (params != NULL){
            var_b = (bencoded_t *) get_hash_table(params, var_name);
            if (var_b != NULL){
                found = TRUE;
                stack[i] = get_bencoded_integer(var_b);
            }
        }
        if (!found){
            var_b = get_var(frame, var_name);
            
            // Array variable
            llist_t *posList = get_var_dim(var_b);
            if (posList != NULL){
                base_pos = add_to_stack(&stack, &stack_size, posList->first,
                                         locations, frame, heap, lfline);

                stack[i] = base_pos;
            }
            stack[i] = 0;

        }
        insert_hash_table(locations, var_name, pos_p);
        var = var->next;
        i++;
    }

    /* **************************************** */
    /* Code interpretation                      */
    return_value = execute_code_list(bm, locations, frame, code, lfline, &stack,
                                     heap, stack_size, enviroment_extend);
   

    free_hash_table(locations, free, NULL);
    if (parent_stack != NULL){
        *parent_stack = (int *) realloc(stack, former_stack_size * sizeof(int));
    }
    else{
        free(stack);
    }
    
    return return_value;
}


/**
 * Description: Launches the interpreter over a function frame.
 *
 * @param bm The bytecode manager.
 * @param frame The frame to be executed.
 * @param enviroment_extend A pointer to a function which receives the unknown 
 *                          function calls.
 * 
 * @return The returned value (if any).
 * 
 */
int launch(bytecode_manager_t *bm, bencoded_t *frame,
             int (*enviroment_extend) (int, int*, char*)){

    int value;
    llist_t *code = get_code_from_frame(frame);
    assert(code != NULL);
    
    lifeline_t lfline = (lifeline_t) malloc(sizeof(struct lifeline));

    assert(lfline != NULL);

    lfline->parent = NULL;
    lfline->frame = get_root_frame(bm);
    lfline->locations = NULL; /** @todo Correct when implemented heap */

    value = execute(bm, frame, code, lfline, NULL, 0, NULL, enviroment_extend,
                      NULL, NULL, NULL);
                      
    free(lfline);

    return value;

}

#endif
