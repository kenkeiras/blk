#ifndef BASIC_INTERPRETER_C
#define BASIC_INTERPRETER_C

#include "interpreter.h"

/**
 * @file basic_interpreter.c
 * 
 * @brief A basic BLK interpreter.
 * 
 */


/**
 * Description: Prints the function name and parameters in C style.
 * 
 * @param param_num Number of parameters.
 * @param params Parameter array.
 * @param fname Function name.
 * 
 * @return It always return 0.
 * 
 */
int enviroment_helper(int param_num, int *params, char *fname){
    int i;

    if (strcmp(fname, "putchar") == 0){
        assert(param_num == 1);
        putchar(params[0]);
    }
    else{
        printf("Catched call: %s(", fname);
        for (i = 0; i < param_num; i++){
            printf("%i", params[i]);
            if (i + 1 < param_num){
                printf(", ");
            }
        }
        printf(");\n");
    }
    return 0;
}


int main(int argc, char**argv){
    if (argc != 2){
        printf("%s <code file>\n", argv[0]);
        return 0;
    }
    
    bytecode_manager_t *bm = create_bytecode_manager(argv[1]);
    
    if (bm == NULL){
        printf("Error reading %s\n", argv[1]);
        return 1;
    }

    bencoded_t *b = get_entry_point(bm);
    if (b == NULL){
        printf("Error: Entry point not found\n");
        return 1;
    }
    
    launch(bm, b, enviroment_helper);

    free_bytecode_manager(bm);
    
    return 0;
}


#endif
