#ifndef C_TOKENS_H
#define C_TOKENS_H

#include <stdio.h>
#include "linked_list.h"
/**
 * @file c_token.h
 * 
 * @brief C token management.
 * 
 */
 
/**
 * Data types.
 * 
 */
/**
 * C token type.
 * 
 */
typedef enum{
    // Special
    NONE,
    END_OF_FILE,
    LINE_FEED, // \n
   
    STRING, 
    CHARACTER,
    INTEGER,
    FLOAT_N,
   
    HASH,       // #
    COLON,      // :
    SEMICOLON,  // ;
    DOT,        // ,
    COMMA,      // ,
    LT,         // <
    GT,         // >

    // C preprocessor tokens
    DEFINE,   // define
    UNDEFINE, // undef
    INCLUDE,  // include
    IFDEF,    // ifdef
    IFNDEF,   // ifndef
    ENDIF,    // endif
    WARNING,  // warning
    ERROR,    // error
    
    // Miscellaneous
    IF,       // if
    ELIF,     // elif
    ELSE,     // else
    
    OPEN_PARENS,  // (
    CLOSE_PARENS, // )
    
    BACKSLASH,    // '\' 

    PLUS,      // +
    MINUS,     // -
    PRODUCT,   // *
    DIVISION,  // /
    BOOL_AND,  // &
    LOGIC_AND, // &&
    BOOL_OR,   // |
    LOGIC_OR,  // ||
    BOOL_XOR,  // ^


    // C tokens
    WHILE,  // while
    FOR,    // for
    DO,     // do
    SWITCH, // switch

    OPEN_BRACE,  // {
    CLOSE_BRACE, // }

    BREAK,      // break
    CONTINUE,   // continue
    RETURN,     // return
    CASE,       // case
    GOTO,       // goto

    CONST,      // const
    STATIC,     // static
    AUTO,       // auto
    EXTERN,     // extern
    REGISTER,   // register
    SIGNED,     // signed
    UNSIGNED,   // unsigned
    VOLATILE,   // volatile
    
    SIZEOF,     // sizeof
    TYPEDEF,    // typedef
    
    CHAR,   // char
    DOUBLE, // double
    FLOAT,  // float
    INT,    // int
    LONG,   // long
    SHORT,  // short
    VOID,   // void

    ENUM,   // enum
    STRUCT, // struct
    UNION,  // union

    VARIABLE // myVariable
} c_token_type;


typedef enum data_representation {
    CHARACTER_R,
    STRING_R,
    INTEGER_R,
    FLOAT_R
}data_representation_t;


/**
 * C token
 * 
 */
typedef struct {
    c_token_type type;
    int line;
    int line_end;
    int column;
    int column_end;
    data_representation_t representation;
    union{
        char   c;
        char  *s;
        int    i;
        float  f;
    }content;
} c_token;


/**
 * Description: Duplicates the data inside a token pointer.
 * 
 * @param token The data to duplicate
 * 
 * @return A new token pointer with the duplicate data.
 * 
 */
c_token* copy_token(c_token token);


/**
 * Description: Writes the textual representation of token on a file.
 * 
 * @param f     File to read to.
 * @param token The token to represent.
 * 
 */
void print_token(FILE* f, c_token token);


/**
 * Description: Frees a allocated token.
 * 
 * @param token The allocated token to free the content from.
 * 
 */
void free_token_ptr(c_token* token);


/**
 * Description: Frees a token content.
 * 
 * @param token The token to free the content from.
 * 
 */
void free_token(c_token token);


/**
 * Description: Checks if a char is a whitespace.
 * 
 * @param c The char to evaluate.
 * 
 * @return True if c is whitespace, else False.
 * 
 */
int is_whitespace(char c);


/**
 * Description: Reads a token from a file.
 * 
 * @param f File to read tokens from.
 * @param line Line number.
 * @param column Column number.
 * 
 * @return The next c_token.
 * 
 */
c_token read_token(FILE* f, int line, int column);


/**
 * Description: Reads a token line from a file. Must free it after use.
 * 
 * @param f File to read tokens from.
 * @param line Line number.
 * @param col Base column.
 * 
 * @return A linked list of c_tokens.
 * 
 */
llist_t* read_token_line(FILE* f, int *line, int col);


#endif
