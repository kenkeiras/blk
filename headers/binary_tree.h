#ifndef BINARY_TREE_H
#define BINARY_TREE_H


#include <assert.h>
#include <stdlib.h>
#include <string.h>

/**
 * @file binary_tree.h
 * 
 * @brief Binary search tree.
 * 
 * Holds a tree of key-content pairs.
 */


/**
 * Data types.
 *
 */
/**
 * Binary tree.
 *
 */
typedef struct binary_tree binary_tree_t;

/**
 * Binary tree node.
 *
 */
typedef struct binary_tree_node binary_tree_node_t;


/**
 * Description: Creates a binary tree.
 *
 * @return A void binary tree.
 *
 */
binary_tree_t *create_binary_tree();


/**
 * Description: Insert into binary tree.
 * 
 * @param t Binary tree.
 * @param key The key for which the content is to be search.
 * @param content The content to insert.
 *
 */
void insert_binary_tree(binary_tree_t *t, char *key, void *content);


/**
 * Description: Retrieves an element from a binary tree.
 *
 * @param t The binary tree to retrieve the data from.
 * @param key The key associated to the content.
 *
 * @return The content associated to the key or NULL if not found.
 *
 */
void *get_binary_tree(binary_tree_t *t, char *key);

/**
 * Description: Removes a node from the binary tree.
 *
 * @param t The binary tree to remove the data from.
 * @param node The node to be removed.
 * @param free_content_f A function to free the value (NULL for none).
 * @param free_key_f A function to free the key (NULL for none).
 *
 */
void rm_binary_tree_node(binary_tree_t *t,
			 binary_tree_node_t *node,
			 void (* free_content_f)(void *),
			 void (* free_key_f)(char *));


/**
 * Description: Removes a node from the binary tree based on its key.
 *
 * @param t The binary tree to remove the data from.
 * @param key The key of the element to be removed.
 * @param free_content_f A function to free the value (NULL for none).
 * @param free_key_f A function to free the key (NULL for none).
 *
 */
void rm_binary_tree(binary_tree_t *t,
		    char *key,
		    void (* free_content_f)(void *),
		    void (* free_key_f)(char *));


/**
 * Description: Frees a binary tree.
 *
 * @param t The tree to be freed.
 * @param free_content_f A function to free the values (NULL for none).
 * @param free_key_f A function to free the keys (NULL for none).
 *
 */
void free_binary_tree(binary_tree_t *t,
		      void (* free_content_f)(void *),
		      void (* free_key_f)(char *));

#endif
