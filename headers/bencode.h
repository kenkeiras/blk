#ifndef BENCODE_H
#define BENCODE_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "hash_table.h"
#include "linked_list.h"

/**
 * @file bencode.h
 * 
 * @brief Bencode reader, writer and structures.
 * 
 */

/* Data structures */
/**
 * Enumerated of bencoded type
 * (integer, string, list or dictionary).
 * 
 */
enum bencoded_type{
    INT_TYPE,
    STR_TYPE,
    LIST_TYPE,
    DICT_TYPE,
};


/**
 * A single bencoded node.
 * 
 */
typedef struct {
    enum bencoded_type type;
    union{
        void *p;
        long  n;
        
    } v;
}bencoded_t;

/**
 * For internal usage.
 */
struct binary_string{
    int size;
    char *s;
};


/* Functions */
/**
 * Description: Reads a bencoded file.
 * 
 * @param f Pointer to the file to be read (assorted to be open with 'b').
 * 
 * @return Bencoded element.
 * 
 */
bencoded_t * read_bencoded(FILE *f);


/**
 * Description: Reads a bencoded file.
 * 
 * @param f Pointer to the file to be read (assorted to be open with 'b').
 * @param size Reader buffer size.
 * 
 * @return Bencoded element.
 * 
 */
bencoded_t * read_bencoded_with_buffer_size(FILE *f, int size);


/**
 * Description: Creates a bencoded dictionary.
 * 
 * @return Bencoded dictionary.
 * 
 */
bencoded_t * create_bencoded_dictionary();


/**
 * Description: Creates a bencoded list.
 * 
 * @return Bencoded dictionary.
 * 
 */
bencoded_t * create_bencoded_list();


/**
 * Description: Creates a bencoded integer.
 * 
 * @return Bencoded integer.
 * 
 */
bencoded_t * create_bencoded_integer();


/**
 * Description: Creates a bencoded integer with a specific value.
 * 
 * @return Bencoded integer.
 * 
 */
bencoded_t * create_bencoded_integer_from(int value);


/**
 * Description: Obtains a pointer to the string of a bencoded string.
 * 
 * @param b Bencoded string to be "unwrapped".
 * 
 * @return A pointer to the actual string.
 * 
 */
char *get_bencoded_string(bencoded_t *b);


/**
 * Description: Obtains a pointer to the linked list from a bencoded list.
 * 
 * @param b Bencoded list to be "unwrapped".
 * 
 * @return A pointer to the actual list.
 * 
 */
llist_t *get_bencoded_llist(bencoded_t *b);


/**
 * Description: Obtains a integer from a bencoded structure.
 * 
 * @param b Bencoded integer to be "unwrapped".
 * 
 * @return The integer contained in b.
 * 
 */
int get_bencoded_integer(bencoded_t *b);


/**
 * Description: Obtains a hash table from a bencoded structure.
 * 
 * @param b Bencoded hash table to be "unwrapped".
 * 
 * @return The hash table contained in b.
 * 
 */
hash_table_t get_bencoded_dictionary(bencoded_t *b);


/**
 * Description: Frees a bencoded data tree.
 * 
 * @param b bencoded data.
 * 
 */
void free_bencoded(bencoded_t * b);

#endif
