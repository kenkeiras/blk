#ifndef BYTECODE_MANAGER_H
#define BYTECODE_MANAGER_H

#include "bencode.h"
#include "hash_table.h"
#include "linked_list.h"

/**
 * @file bytecode_manager.h
 *
 * @brief Bytecode data manager.
 * 
 */

/**
 * Built-in operation codes
 * 
 */
#define RETURN_OP       1  // One parameter  (return x)
#define ASSIGNATION_OP  2  // One parameter  (x = ... )
#define GET_ADRESS_OP   3  // One parameter  ( &x     )
#define DEREFERENCE_OP  4  // One parameter  ( *x     )

#define ADD_OP          5  // Two parameters ( x + y  )
#define SUB_OP          6  // Two parameters ( x - y  )
#define MUL_OP          7  // Two parameters ( x * y  )
#define DIV_OP          8  // Two parameters ( x / y  )

#define EQ_OP           9  // Two parameters ( x == y )
#define NE_OP           10 // Two parameters ( x != y )
#define LT_OP           11 // Two parameters ( x <  y )
#define GT_OP           12 // Two parameters ( x >  y )
#define LE_OP           13 // Two parameters ( x <= y )
#define GE_OP           14 // Two parameters ( x >= y )

#define NEGATION_OP     15 // One parameter  ( !x     )


/**
 *  Branch type codes
 * 
 */
#define IF_BRANCH        1
#define WHILE_BRANCH     2
#define DO_WHILE_BRANCH  3
#define ELSE_BRANCH      4


/**
 * Instruction group codes
 * 
 */
#define NO_GROUP      0 // Invalid group
#define OP_GROUP      1 // A = B
#define BRANCH_GROUP  2 // if|while|do..while(){}


/**
 * Data holder type
 * 
 */
typedef struct{
    bencoded_t * tree;
}bytecode_manager_t;


/**
 * Description: Create a new bytecode manager descriptor.
 * 
 * @return The bytecode manager descriptor.
 * 
 */
bytecode_manager_t *create_bytecode_manager();


/**
 * Description: Create a new bytecode manager descriptor.
 * 
 * @param f The bytecode file.
 * 
 * @return The bytecode manager descriptor.
 * 
 */
bytecode_manager_t *create_bytecode_manager_from_file(FILE *f);


/**
 * Description: Create a new bytecode manager descriptor.
 * 
 * @param fname The bytecode file name.
 * 
 * @return The bytecode manager descriptor.
 * 
 */
bytecode_manager_t *create_bytecode_manager_from_file_name(char *fname);


/**
 * Description: Returns the root frame of a bytecode manager descriptor.
 * 
 * @param bm The bytecode manager descriptor.
 * 
 * @return The root frame.
 * 
 */
bencoded_t *get_root_frame(bytecode_manager_t *bm);


/**
 * Description: Adds a function to the frame
 * 
 * @param bm The bytecode manager descriptor.
 * @param function_name The name of the function name.
 * @param function_type The type of the function.
 * @param function_parameters The parameters of the function.
 * @param frame The frame where the function is going to be inserted.
 * 
 * @return The inserted function frame.
 * 
 */
bencoded_t * add_function(bytecode_manager_t *bm,
                  char * function_name,
                  bencoded_t *function_type,
                  bencoded_t *function_parameters,
                  bencoded_t *frame);


/**
 * Description: Obtains the bytecode entry point.
 * 
 * @param bm Code associated bytecode manager.
 * 
 * @return The entry point function bencoded tree.
 * 
 */
bencoded_t * get_entry_point(bytecode_manager_t *bm);


/**
 * Description: Returns the operation list from a frame.
 * 
 * @param frame The frame containing the code.
 * 
 * @return The code operations linked list.
 * 
 */
llist_t *get_code_from_frame(bencoded_t *frame);


/**
 * Description: Extracts the operation group from a bencoded operation.
 * 
 * @param op The operation.
 * 
 * @return The group code of the operation.
 * 
 */
int get_op_group(bencoded_t *op);


/**
 * Description: Extracts the operation code from a bencoded operation.
 * 
 * @param op The operation.
 * 
 * @return The operation code of the operation.
 * 
 */
bencoded_t *get_op_code(bencoded_t *op);


/**
 * Description: Returns a list of the operation parameters.
 * 
 * @param op The operation.
 * 
 * @return A linked list contaning the operation parameters.
 * 
 */
bencoded_t *get_op_params(bencoded_t *op);


/**
 * Description: Obtains the variable where to store the results of 
 *  the operation.
 * 
 * @param op The operation.
 * 
 * @return A bencoded variable reference.
 * 
 */
bencoded_t *get_op_return_var(bencoded_t *op);


/**
 * Description: Returns the operation function name.
 * 
 * @param op The operation.
 * 
 * @return The function name.
 * 
 */
char *get_op_function_name(bencoded_t *op);


/**
 * Description: Returns the branching condition of a operation.
 * 
 * @param op The branching operation where the condition is evaluated.
 * 
 * @return The condition variable.
 * 
 */
bencoded_t *get_op_condition(bencoded_t *op);


/**
 * Description: Returns the operations to take when evaluating
 *  the branching condition only after the first iteration.
 * 
 * @param branch The bencoded branching operation.
 * 
 * @return The operations to take.
 * 
 */
llist_t* get_branch_step_code(bencoded_t *branch);


/**
 * Description: Returns the operations to take inside a loop or conditional.
 * 
 * @param branch The bencoded branching operation.
 * 
 * @return The operations to take.
 * 
 */
llist_t* get_branch_code(bencoded_t *branch);


/**
 * Description: Returns the branch type of a operation.
 * 
 * @param op The bencoded operation
 * 
 * @return It's branch type
 * 
 */
int get_branch_type(bencoded_t *op);


/**
 * Description: Returns the operations to take before evaluating
 *  the branching condition.
 * 
 * @param branch The bencoded branching operation.
 * 
 * @return The operations to take.
 * 
 */
llist_t* get_branch_eval_code(bencoded_t *branch);


/**
 * Description: Returns the dimension list of a variable.
 * 
 * @param var The variable to look in.
 * 
 * @return A linked list containing the secuence of sizes.
 * 
 */
llist_t *get_var_dim(bencoded_t *var);


/**
 * Description: Returns the consecuent array location list of a variable
 *   reference.
 * 
 * @param var The variable to look in.
 * 
 * @return A linked list containing the secuence of references.
 * 
 */
llist_t *get_var_pos(bencoded_t *var);


/**
 * Description: Obtains the name of a bencoded variable.
 * 
 * @param var The variable to obtain the name from.
 * 
 * @return The name of the variable or NULL if not found.
 * 
 */
char *get_var_name(bencoded_t *var);


/**
 * Description: Returns either or not a frame contains a variable.
 * 
 * @param var_name The variable name.
 * @param frame    The code frame.
 * 
 * @return 0 if it doesn't contains the variable and 1 if it does.
 * 
 */
int frame_has_variable(char *var_name, bencoded_t *frame);


/**
 * Description: Looks for a function in a frame.
 * 
 * @param fun_name The function name.
 * @param frame    The code frame.
 * 
 * @return The bencoded function or NULL if is not found.
 * 
 */
bencoded_t *get_frame_function(char *fun_name, bencoded_t *frame);


/**
 * Description: Returns a list of the variable names in the frame.
 * 
 * @param frame    The code frame.
 * 
 * @return The string list of variable names.
 * 
 */
llist_t *get_frame_var_names(bencoded_t *frame);

/**
 * Description: Returns a list of the function parameters.
 * 
 * @param function The function frame.
 * 
 * @return The variable list of parameters.
 * 
 */
llist_t *get_function_params(bencoded_t *function);

/**
 * Description: Returns a variable object from a code frame.
 * 
 * @param frame The code frame.
 * @param var_name The variable name.
 * 
 * @return The bencoded variable.
 * 
 */
bencoded_t *get_var(bencoded_t *frame, char *var_name);


/**
 * Describe: Checks if a bencoded variable points to some actual one.
 * 
 * @param var The variable to check.
 * 
 * @return 0 if it's a void value, 1 elsewhere.
 * 
 */
int is_meaningfull_variable(bencoded_t *var);


/**
 * Description: Frees the bytecode manager memory.
 * 
 * @param bm The bytecode manager to free.
 * 
 */
void free_bytecode_manager(bytecode_manager_t *bm);


#endif

