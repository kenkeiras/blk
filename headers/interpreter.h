#ifndef INTERPRETER_H
#define INTERPRETER_H

#include "bytecode_manager.h"

/**
 * @file interpreter.h
 * 
 * @brief Functions and structures to write a BLK interpreter.
 * 
 */

#ifndef TRUE
    #define TRUE 1
#endif
#ifndef FALSE
    #define FALSE 0
#endif

/**
 * Structure that connects a frame with it's parent
 * 
 */
struct lifeline{
    struct lifeline * parent;
    bencoded_t *frame;
    hash_table_t locations;
};

typedef struct lifeline * lifeline_t;


/**
 * Description: Obtains the value of a bencoded variable.
 * 
 * @param var The variable to "unwrap".
 * @param locations The current frame value table.
 * @param frame  The current code frame.
 * @param lfline The connection to the parent frames.
 * @param stack Local variable memory.
 * @param heap Static content memory.
 * @param stack_size The size of the local memory.
 * 
 * @return The integer value of 'var'.
 * 
 */
int get_value(bencoded_t *var,
              hash_table_t locations,
              bencoded_t  *frame,
              lifeline_t lfline,
              int *stack,
              int *heap,
              int stack_size);


/**
 * Description: Launches the interpreter over a function frame.
 *
 * @param bm The bytecode manager.
 * @param frame The frame to be executed.
 * @param enviroment_extend A pointer to a function which receives the unknown 
 *                          function calls.
 * 
 * @return The returned value (if any).
 * 
 */
int launch(bytecode_manager_t *bm, bencoded_t *frame,
             int (*enviroment_extend) (int, int*, char*));


#endif

