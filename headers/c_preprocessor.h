#ifndef C_PREPROCESSOR_H
#define C_PREPROCESSOR_H

#include <stdio.h>
#include "linked_list.h"
#include "c_tokens.h"
/**
 * @file c_preprocessor.h
 * 
 * @brief C preprocessing functions and structures.
 * 
 */
 
/**
 * Data types.
 * 
 */
/**
 * Preprocessed data type.
 * 
 */
typedef llist_t preprocessed_data_t;

/**
 * C preprocessing manager data type.
 * 
 */
struct c_preprocessor;
typedef struct c_preprocessor c_preprocessor_t;

#define INCLUDE_CPP_DIRECTIVE "include"

#ifndef __WIN32__
  #include <unistd.h>
  #define DEFAULT_INCLUDE_PATH "/usr/include/"
#else
  #error "Unknown include path :P"
#endif

/**
 * Description: passes a file through a C preprocessor.
 * 
 * @param cpp   The C preprocessor structure.
 * @param fname The name of the file to be preprocessed.
 * @param inc_paths #Include file paths.
 * 
 * @return The preprocessed data of the file.
 * 
 */
preprocessed_data_t* preprocess_file(c_preprocessor_t *cpp,
                                     char *fname,
                                     llist_t *inc_paths);


/**
 * Description: stores preprocessed data in a file.
 * 
 * @param f The file where the info is going to be stored.
 * @param pd The preprocessed data to be stored.
 * 
 */
void output_preprocessed(FILE *f, preprocessed_data_t* pd);

#endif
