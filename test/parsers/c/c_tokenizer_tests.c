#include "CUnit/Basic.h"
#include "c_tokens.h"
#include <string.h>
#include <stdlib.h>
#include <assert.h>

#include <unistd.h>

#define TMPNAME "__tmp__"

void single_token_test(){
    
    c_token_type tokens[] = { 
            STRING, CHARACTER, INTEGER, FLOAT_N,
            HASH, COLON, SEMICOLON, DOT, COMMA, LT, GT,
            DEFINE, UNDEFINE, INCLUDE, IFDEF, IFNDEF, WARNING, ERROR,
            IF, ELIF, ELSE, ENDIF,
            OPEN_PARENS, CLOSE_PARENS, PLUS, MINUS, PRODUCT, DIVISION,
            LOGIC_AND, LOGIC_OR, BOOL_AND, BOOL_OR, BOOL_XOR, BACKSLASH,
            
            WHILE, FOR, DO, SWITCH, 
            OPEN_BRACE, CLOSE_BRACE, 
            BREAK, CONTINUE, RETURN, CASE, GOTO,       
            CONST, STATIC, AUTO, EXTERN, REGISTER, SIGNED, UNSIGNED, VOLATILE,   
            SIZEOF, TYPEDEF,    
            CHAR, DOUBLE, FLOAT, INT, LONG, SHORT, VOID,   
            ENUM, STRUCT, UNION,
            VARIABLE, VARIABLE, VARIABLE, VARIABLE };

    char* token_list[] = {
            "\"asd\"", "'a'", "1", "0.5",
            "#", ":", ";", ".", ",", "<", ">",
            "define", "undef", "include", "ifdef", "ifndef", "warning", "error",
            "if", "elif", "else", "endif",
            "(", ")", "+", "-", "*", "/", "&&", "||", "&", "|", "^", "\\",
            "while", "for", "do", "switch",
            "{", "}",
            "break", "continue", "return", "case", "goto",
            "const", "static", "auto", "extern", "register",
                                            "signed", "unsigned", "volatile",
            "sizeof", "typedef",
            "char", "double", "float", "int", "long", "short", "void",
            "enum", "struct", "union",
            "auto_", "test", "blk", "suites", NULL};

    char* token_val[] = {
            "asd", "'a'", "1", "0.5",
            "#", ":", ";", ".", ",", "<", ">",
            "define", "undef", "include", "ifdef", "ifndef", "warning", "error",
            "if", "elif", "else", "endif",
            "(", ")", "+", "-", "*", "/", "&&", "||", "&", "|", "^", "\\",
            "while", "for", "do", "switch",
            "{", "}",
            "break", "continue", "return", "case", "goto",
            "const", "static", "auto", "extern", "register",
                                            "signed", "unsigned", "volatile",
            "sizeof", "typedef",
            "char", "double", "float", "int", "long", "short", "void",
            "enum", "struct", "union",
            "auto_", "test", "blk", "suites", NULL};


    int i;
    for (i = 0; token_list[i] != NULL;i++){
        FILE* f = fopen(TMPNAME, "wt");
        fwrite(token_list[i], sizeof(char), strlen(token_list[i]), f);
        fclose(f);
        
        f = fopen(TMPNAME, "rt");
        c_token token = read_token(f, 0, 0);
        fclose(f);
        
        CU_ASSERT(token.type == tokens[i]);

        if ((token.representation == STRING_R) && (token.content.s != NULL)){
            CU_ASSERT(strcmp(token.content.s, token_val[i]) == 0);
        }
        free_token(token);
    }
    
    unlink(TMPNAME);
}

int add_c_tokenizer_test_suite(){
    CU_pSuite pSuite = NULL;

    /* add a suite to the registry */
    pSuite = CU_add_suite("C Tokenizer", NULL, NULL);
    if (NULL == pSuite) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    /* add the tests to the suite */
    if ((NULL == CU_add_test(pSuite, "single token parsing", single_token_test))){

        CU_cleanup_registry();
        return CU_get_error();
    } 

    return 0;
}
