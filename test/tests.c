#include <stdio.h>
#include "CUnit/Basic.h"
#include "hash_table_tests.h"
#include "linked_list_tests.h"
#include "binary_tree_tests.h"
#include "c_tokenizer_tests.h"

int main(int argc, char** argv){

    int code;

    /* initialize the CUnit test registry */
    if (CUE_SUCCESS != CU_initialize_registry()){
        return CU_get_error();
    }

    /* Add the test suites */
    if ((code = add_linked_list_test_suite()) != 0){
        return code;
    }
    if ((code = add_binary_tree_test_suite()) != 0){
        return code;
    }
    if ((code = add_hash_table_test_suite()) != 0){
        return code;
    }
    if ((code = add_c_tokenizer_test_suite()) != 0){
        return code;
    }

    /* Run all tests using the CUnit Basic interface */
    CU_basic_set_mode(CU_BRM_VERBOSE);
    CU_basic_run_tests();
    CU_cleanup_registry();
    return CU_get_error();
}

