#ifndef HASH_TABLE_TESTS_H
#define HASH_TABLE_TESTS_H


/**
 * @file hash_table_tests.h
 * 
 * @brief Hash table unity tests.
 * 
 */
 
/**
 * Description: Adds the hash table test suite to the registry.
 * 
 * @return 0 or error code.
 * 
 */
int add_hash_table_test_suite();

#endif
