#ifndef LINKED_LIST_TESTS_H
#define LINKED_LIST_TESTS_H


/**
 * @file linked_list_tests.h
 * 
 * @brief Linked list unity tests.
 * 
 */
 
/**
 * Description: Adds the linked list test suite to the registry.
 * 
 * @return 0 or error code.
 * 
 */
int add_linked_list_test_suite();

#endif
