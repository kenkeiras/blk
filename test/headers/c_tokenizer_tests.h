#ifndef C_TOKENIZER_TESTS_H
#define C_TOKENIZER_TESTS_H


/**
 * @file c_tokenizer_tests.h
 * 
 * @brief C tokenizer unity tests.
 * 
 */
 
/**
 * Description: Adds the c tokenizer test suite to the registry.
 * 
 * @return 0 or error code.
 * 
 */
int add_c_tokenizer_test_suite();

#endif

