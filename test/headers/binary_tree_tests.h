#ifndef BINARY_TREE_TESTS_H
#define BINARY_TREE_TESTS_H


/**
 * @file binary_tree_tests.h
 * 
 * @brief Binary tree unity tests.
 * 
 */
 
/**
 * Description: Adds the binary tree test suite to the registry.
 * 
 * @return 0 or error code.
 * 
 */
int add_binary_tree_test_suite();

#endif
