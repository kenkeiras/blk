#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <assert.h>

#include "linked_list.h"
#include "CUnit/Basic.h"

#ifndef NUM_RANDOM_TEST
#define NUM_RANDOM_TEST 100
#endif

/**
 * Description: Tries to insert NUM_RANDOM_TEST elements in a list.
 * 
 */
void llist_test_insert_random_pointer(){
    int test_num = NUM_RANDOM_TEST;
    
    llist_t * l = create_llist();
    void *p;

    srand(time(NULL));

    int i;
    /* Inserts pointers in the linked list */ 
    for (i = 0; i < test_num; i++){
        
        p = (void *) (i * i);

        add_to_llist(l, p);

    }

    llist_node_t *node = l->first;
    
    /* Then check if they are correctly stored */
    for (i = 0; node != NULL; i++){

        p = node->element;

        CU_ASSERT(p == ((void*)( i * i)));
        
        assert(i < test_num);
        node = node->next;
    }
    
    /* Check the integer key size */
    CU_ASSERT(l->size == NUM_RANDOM_TEST);
    
    free_llist(l, NULL);
}


int add_linked_list_test_suite(){
    CU_pSuite pSuite = NULL;

    /* add a suite to the registry */
    pSuite = CU_add_suite("Linked list test suite", NULL, NULL);
    if (NULL == pSuite) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    /* add the tests to the suite */
    if ((NULL == CU_add_test(
                             pSuite, "data insertion",
                             llist_test_insert_random_pointer))
                             ){
           
        CU_cleanup_registry();
        return CU_get_error();
    }   

    return 0;
}

/* Just this part is being tested */
#ifdef STANDALONE

int main(int argc, char**argv){

    /* initialize the CUnit test registry */
    if (CUE_SUCCESS != CU_initialize_registry()){
        return CU_get_error();
    }

    add_linked_list_test_suite();

    /* Run all tests using the CUnit Basic interface */
    CU_basic_set_mode(CU_BRM_VERBOSE);
    CU_basic_run_tests();
    CU_cleanup_registry();
    return CU_get_error();
}

#endif

