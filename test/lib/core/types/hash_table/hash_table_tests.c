#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <assert.h>


#include "hash_table.h"
#include "CUnit/Basic.h"

#ifndef NUM_RANDOM_TEST
#define NUM_RANDOM_TEST 100
#endif

#ifndef TEST_HASH_SIZE
#define TEST_HASH_SIZE 65537
#endif

/**
 * Description: Tries to insert NUM_RANDOM_TEST elements in a table with defined
 * size.
 * 
 * @param size The number of fields of the hash table.
 * 
 */
void test_insertion_with_size(int size){
    hash_table_t t = create_hash_table(size);
    char *x;
    void *p;

    srand(1);

    int i;
    /* Inserts random pointers both indexed by integers
     *  and it's string representations */ 
    for (i = 0; i < NUM_RANDOM_TEST; i++){

        x = (char *) malloc(sizeof(char) * 20);
        sprintf(x, "%i", i);
        
        p = (void *) rand();

        insert_hash_table(t, x, p);

    }

    srand(1);

    /* Then check if they are correctly stored compairing them */
    x = (char *) malloc(sizeof(char) * 20);
    for (i = 0; i < NUM_RANDOM_TEST; i++){

        sprintf(x, "%i", i);
        p = get_hash_table(t, x);

        CU_ASSERT(p == (void *) rand());
	if ((int)p & 1){
	    rm_hash_table(t, x, NULL, (void*)(char*) free);
	}
    }
    
    /* Check the  key size */
    CU_ASSERT(get_hash_table_str_keys(t)->size == NUM_RANDOM_TEST);

    free(x);
    free_hash_table(t, NULL, (void (*) (char *)) free);
}


/**
 * Description: Just tries to insert data in the table.
 * 
 */
void testRANDOM_POINTER(){
    test_insertion_with_size(TEST_HASH_SIZE);
}


/**
 * Description: Tries to insert more data than fields in the table.
 * 
 */
void testMORE_DATA(){
    assert(NUM_RANDOM_TEST / 10 > 0);

    /* The size is not optimal, but works for testing */
    test_insertion_with_size(NUM_RANDOM_TEST / 10);
}


int add_hash_table_test_suite(){
    CU_pSuite pSuite = NULL;

    /* add a suite to the registry */
    pSuite = CU_add_suite("Hash table test suite", NULL, NULL);
    if (NULL == pSuite) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    /* add the tests to the suite */
    if ((NULL == CU_add_test(pSuite, "data insertion and removal",
                             testRANDOM_POINTER)) || 
        (NULL == CU_add_test(pSuite, "more data than size",
                             testMORE_DATA))){
           
        CU_cleanup_registry();
        return CU_get_error();
    }   

    return 0;
}

/* Just this part is being tested */
#ifdef STANDALONE

int main(int argc, char**argv){

    /* initialize the CUnit test registry */
    if (CUE_SUCCESS != CU_initialize_registry()){
        return CU_get_error();
    }

    add_hash_table_test_suite();

    /* Run all tests using the CUnit Basic interface */
    CU_basic_set_mode(CU_BRM_VERBOSE);
    CU_basic_run_tests();
    CU_cleanup_registry();
    return CU_get_error();
}

#endif
