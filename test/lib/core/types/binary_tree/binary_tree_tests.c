#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <assert.h>

#include "binary_tree.h"
#include "CUnit/Basic.h"

#ifndef NUM_RANDOM_TEST
#define NUM_RANDOM_TEST 100
#endif

/**
 * Description: Tries to insert NUM_RANDOM_TEST elements in a binary tree.
 * 
 */
void binary_tree_test_insert_random_pointer(){
    char *x;
    void *p;

    binary_tree_t *t = create_binary_tree();

    srand(1);

    int i;
    /* Inserts random pointers both indexed by integers
     *  and it's string representations */ 
    for (i = 0; i < NUM_RANDOM_TEST; i++){

        x = (char *) malloc(sizeof(char) * 20);
        sprintf(x, "%i", i);
        
        p = (void *) rand();

        insert_binary_tree(t, x, p);

    }

    srand(1);

    /* Then check if they are correctly stored compairing them */
    x = (char *) malloc(sizeof(char) * 20);
    for (i = 0; i < NUM_RANDOM_TEST; i++){

        sprintf(x, "%i", i);
        p = get_binary_tree(t, x);

        CU_ASSERT(p == (void *) rand());
	if ((int)p & 1){
	    rm_binary_tree(t, x, NULL, (void *)(void*) free);
	}
    }
    
    free(x);
    free_binary_tree(t, NULL, (void *)(void*) free);
}


int add_binary_tree_test_suite(){
    CU_pSuite pSuite = NULL;

    /* add a suite to the registry */
    pSuite = CU_add_suite("Binary tree test suite", NULL, NULL);
    if (NULL == pSuite) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    /* add the tests to the suite */
    if ((NULL == CU_add_test(pSuite, "data insertion and removal",
                             binary_tree_test_insert_random_pointer))){
           
        CU_cleanup_registry();
        return CU_get_error();
    }   

    return 0;
}

/* Just this part is being tested */
#ifdef STANDALONE

int main(int argc, char**argv){

    /* initialize the CUnit test registry */
    if (CUE_SUCCESS != CU_initialize_registry()){
        return CU_get_error();
    }

    add_binary_tree_test_suite();

    /* Run all tests using the CUnit Basic interface */
    CU_basic_set_mode(CU_BRM_VERBOSE);
    CU_basic_run_tests();
    CU_cleanup_registry();
    return CU_get_error();
}

#endif

